import { ReactElement, useContext } from 'react';
import { View } from 'react-native';
import { Button, ButtonTypes, TextComponent } from '@patrollar/mobile-components';
import { AppLogo } from '@patrollar/assets/logos';
import { useForm } from 'react-hook-form';
import {
  ForgotPasswordFormInputs, ForgotPasswordFormSchema, PatrollarContext, Routes,
} from '@patrollar/shared';
import { zodResolver } from '@hookform/resolvers/zod';
import { useNavigation } from '@react-navigation/native';
import {
  buttonStyle,
  StyledWelcomeMessage,
} from '../register/Register.styled';
import ControlledInput from '../../components/inputs/ControlledInput';
import { StyledForgotPasswordContainer, StyledForgotPasswordSection } from './ForgotPassword.styled';
import type { NavigateProps } from '../../navigator/NavigatorProps';

export default function ForgotPassword(): ReactElement {
  const { navigate } = useNavigation<NavigateProps>();
  const { userService } = useContext(PatrollarContext);

  const { handleSubmit, control } = useForm<ForgotPasswordFormInputs>({
    resolver: zodResolver(ForgotPasswordFormSchema),
    mode: 'onBlur',
  });

  async function onSendInstructionsPress(data: ForgotPasswordFormInputs): Promise<void> {
    navigate(Routes.emailConfirmation);
    await userService.forgotPassword(data.email);
  }

  return (
    <StyledForgotPasswordContainer>
      <StyledForgotPasswordSection>
        <AppLogo height={50} />
        <StyledWelcomeMessage bold>Forgot Password</StyledWelcomeMessage>
        <TextComponent>
          Enter your email address below and we&apos;ll
          send you an email with instructions on how to recover your account
        </TextComponent>
      </StyledForgotPasswordSection>
      <StyledForgotPasswordSection>
        <ControlledInput
          placeholder="Email"
          keyboardType="email-address"
          name="email"
          control={control}
        />
        <Button
          style={buttonStyle}
          type={ButtonTypes.primary}
          onPress={handleSubmit(onSendInstructionsPress)}
          title="Send instructions"
        />
      </StyledForgotPasswordSection>
      <View style={{ flex: 3 }} />
    </StyledForgotPasswordContainer>
  );
}
