import styled from 'styled-components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Dimensions, View } from 'react-native';

const StyledForgotPasswordContainer = styled(SafeAreaView)`
  padding: 15px 30px;
  min-height: ${Dimensions.get('screen').height}px;
  justify-content: space-around;
`;

const StyledForgotPasswordSection = styled(View)`
  flex: 1;
  justify-content: space-around;
`;

export { StyledForgotPasswordContainer, StyledForgotPasswordSection };
