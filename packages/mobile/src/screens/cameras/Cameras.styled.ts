import styled from 'styled-components/native';

const StyledCameraContainer = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme.colors.background};
`;

// eslint-disable-next-line import/prefer-default-export
export { StyledCameraContainer };
