import { useRef, ReactElement, Suspense } from 'react';
import {
  CamerasState, Routes, VisibleCameraState, ICamera,
} from '@patrollar/shared';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { useNavigation } from '@react-navigation/native';
import { ActivityIndicator, FlatList, ViewToken } from 'react-native';
import { ErrorBoundary } from '@patrollar/mobile-components';
import { StyledCameraContainer } from './Cameras.styled';
import CameraItemComponent from '../../components/camera-item/CameraItem';
import type { NavigateProps } from '../../navigator/NavigatorProps';

function Cameras(): ReactElement {
  const { navigate } = useNavigation<NavigateProps>();
  const cameras = useRecoilValue(CamerasState);
  const setVisibleCamera = useSetRecoilState(VisibleCameraState);

  function onPlayPress(camera: ICamera): void {
    return setVisibleCamera(camera);
  }

  function onCameraPress(camera: ICamera): void {
    onPlayPress(camera);

    return navigate(Routes.detections, { camera });
  }

  function onListScroll(items: { viewableItems: Array<ViewToken> }): void {
    const { viewableItems } = items;
    const isFirstCameraVisible = viewableItems.find(({ index }) => index === 0);
    const [visibleCamera] = isFirstCameraVisible
      ? viewableItems
      : viewableItems.slice(-1);

    if (visibleCamera) {
      setVisibleCamera(visibleCamera.item);
    }
  }

  const onViewRef = useRef(onListScroll);

  const viewConfigRef = useRef({
    itemVisiblePercentThreshold: 100,
    minimumViewTime: 250,
  });

  function renderItem({ item }: { item: ICamera }): ReactElement {
    return (
      <CameraItemComponent
        camera={item}
        onCameraPress={onCameraPress}
        onCameraPlayPress={onPlayPress}
      />
    );
  }

  function keyExtractor({ id }: ICamera): string {
    return id;
  }

  return (
    <StyledCameraContainer>
      <FlatList
        numColumns={1}
        data={cameras}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        onViewableItemsChanged={onViewRef.current}
        viewabilityConfig={viewConfigRef.current}
      />
    </StyledCameraContainer>
  );
}

export default function SuspendedCameras(): ReactElement {
  return (
    <ErrorBoundary>
      <Suspense fallback={<ActivityIndicator style={{ alignSelf: 'center', flex: 1 }} size="large" />}>
        <Cameras />
      </Suspense>
    </ErrorBoundary>
  );
}
