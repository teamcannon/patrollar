import { ReactElement, useContext, useEffect } from 'react';
import { Button, ButtonTypes, TextComponent } from '@patrollar/mobile-components';
import { useNavigation } from '@react-navigation/native';
import {
  Routes, PatrollarContext, RegisterFormSchema, RegisterFormInputs,
} from '@patrollar/shared';
import { Keyboard, Platform, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { AppLogo } from '@patrollar/assets/logos';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import Animated, { useAnimatedStyle, useSharedValue, withTiming } from 'react-native-reanimated';
import {
  StyledRegisterContainer,
  StyledCenteredContainer,
  StyledPrimaryText,
  StyledWelcomeMessage,
  buttonStyle,
  StyledRegisterFormContainer, inputStyle,
} from './Register.styled';
import type { NavigateProps } from '../../navigator/NavigatorProps';
import ControlledInput from '../../components/inputs/ControlledInput';

const IMAGE_HEIGHT = 50;
const IMAGE_HEIGHT_SMALL = 25;

export default function Register(): ReactElement {
  const { reset } = useNavigation<NavigateProps>();
  const { handleSubmit, control } = useForm<RegisterFormInputs>({
    resolver: zodResolver(RegisterFormSchema),
    mode: 'onBlur',
  });
  const { userService } = useContext(PatrollarContext);
  const offset = useSharedValue(50);

  const animatedStyles = useAnimatedStyle(() => ({
    height: withTiming(offset.value),
  }));

  useEffect(() => {
    function keyboardDidShow(): void {
      offset.value = IMAGE_HEIGHT_SMALL;
    }

    function keyboardDidHide(): void {
      offset.value = IMAGE_HEIGHT;
    }

    const showEventName = Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow';
    const hideEventName = Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide';

    const keyboardShowSub = Keyboard.addListener(showEventName, keyboardDidShow);
    const keyboardHideSub = Keyboard.addListener(hideEventName, keyboardDidHide);

    return () => {
      keyboardShowSub.remove();
      keyboardHideSub.remove();
    };
  }, []);

  function onSignInPress(): void {
    return reset({
      index: 0,
      routes: [{ name: Routes.login }],
    });
  }

  async function onRegisterPress(data: RegisterFormInputs): Promise<void> {
    reset({
      index: 0,
      routes: [{ name: Routes.emailConfirmation }],
    });
    await userService.register(data);
  }

  function getRegisterForm(): ReactElement {
    return (
      <StyledRegisterFormContainer>
        <ControlledInput
          name="email"
          control={control}
          placeholder="Email"
          keyboardType="email-address"
          containerStyle={inputStyle}
        />
        <ControlledInput
          name="phoneNumber"
          control={control}
          placeholder="Phone Number"
          keyboardType="phone-pad"
          containerStyle={inputStyle}
        />
        <ControlledInput
          name="password"
          control={control}
          placeholder="Password"
          secureTextEntry
          containerStyle={inputStyle}
        />
        <ControlledInput
          name="confirmPassword"
          control={control}
          placeholder="Confirm Password"
          secureTextEntry
          containerStyle={inputStyle}
        />
        <Button
          style={buttonStyle}
          type={ButtonTypes.primary}
          onPress={handleSubmit(onRegisterPress)}
          title="Let's get started"
        />
      </StyledRegisterFormContainer>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StyledRegisterContainer
        keyboardVerticalOffset={0}
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        <Animated.View style={animatedStyles}>
          <AppLogo />
        </Animated.View>
        <StyledWelcomeMessage bold>Create Account</StyledWelcomeMessage>
        <TextComponent>And start securing your home</TextComponent>
        <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
          {getRegisterForm()}
        </ScrollView>

      </StyledRegisterContainer>
      <StyledCenteredContainer>
        <TextComponent>Already have an account? </TextComponent>
        <Button type={ButtonTypes.link} onPress={onSignInPress}>
          <StyledPrimaryText bold>
            Sign in
          </StyledPrimaryText>
        </Button>
      </StyledCenteredContainer>
    </SafeAreaView>
  );
}
