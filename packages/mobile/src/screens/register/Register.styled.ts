import styled, { css } from 'styled-components';
import { KeyboardAvoidingView, View } from 'react-native';
import { TextComponent } from '@patrollar/mobile-components';

const StyledRegisterContainer = styled(KeyboardAvoidingView)`
  padding: 10px 30px;
`;

const StyledCenteredContainer = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin: 30px 0;
`;

const StyledPrimaryText = styled(TextComponent)`
  color: #4CBDAD
`;

const StyledWelcomeMessage = styled(TextComponent)`
  font-size: 36px;
  color: #172B4D;
`;

const buttonStyle = css`
  padding: 20px;
  border-radius: 60px;
`;

const inputStyle = css`
  margin: 5px 0;
`;

const StyledRegisterFormContainer = styled(View)`
  margin-top: 10px;
  justify-content: space-around;
`;

export {
  StyledRegisterContainer,
  StyledCenteredContainer,
  StyledPrimaryText,
  StyledWelcomeMessage,
  buttonStyle,
  inputStyle,
  StyledRegisterFormContainer,
};
