import styled from 'styled-components';
import { Dimensions, KeyboardAvoidingView, View } from 'react-native';
import { TextComponent } from '@patrollar/mobile-components';

const StyledResetPasswordContainer = styled(KeyboardAvoidingView)`
  padding: 30px;
  min-height: ${Dimensions.get('screen').height}px;
  justify-content: center;
`;

const StyledWelcomeSection = styled(View)`
  flex: 1;
  justify-content: center;
`;

const StyledResetPasswordForm = styled(View)`
  flex: 1.7;
  justify-content: space-evenly;
`;

const StyledResetPasswordTitle = styled(TextComponent)`
  font-size: 36px;
`;

export {
  StyledResetPasswordContainer,
  StyledWelcomeSection,
  StyledResetPasswordForm,
  StyledResetPasswordTitle,
};
