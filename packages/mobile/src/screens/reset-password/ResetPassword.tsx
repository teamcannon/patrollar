import { ReactElement, useContext } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Button, ButtonTypes, TextComponent } from '@patrollar/mobile-components';
import { AppLogo } from '@patrollar/assets/logos';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  ResetPasswordFormInputs, ResetPasswordFormSchema, PatrollarContext, Routes,
} from '@patrollar/shared';
import { View } from 'react-native';

import type { NavigateProps, ResetPasswordRouteProp } from '../../navigator/NavigatorProps';
import {
  StyledResetPasswordContainer,
  StyledWelcomeSection,
  StyledResetPasswordForm,
  StyledResetPasswordTitle,
} from './ResetPassword.styled';
import ControlledInput from '../../components/inputs/ControlledInput';
import { buttonStyle } from '../login/Login.styled';

export default function ResetPassword(): ReactElement {
  const { params } = useRoute<ResetPasswordRouteProp>();
  const { email } = params;
  const { handleSubmit, control } = useForm<ResetPasswordFormInputs>({
    resolver: zodResolver(ResetPasswordFormSchema),
    mode: 'onBlur',
  });
  const { reset } = useNavigation<NavigateProps>();
  const { userService } = useContext(PatrollarContext);

  async function onResetPassword(data: ResetPasswordFormInputs): Promise<void> {
    reset({
      index: 0,
      routes: [{ name: Routes.login }],
    });
    await userService.resetPassword(email, data.password, data.confirmPassword);
  }

  return (
    <StyledResetPasswordContainer>
      <StyledWelcomeSection>
        <AppLogo style={{ height: 50 }} />
        <StyledResetPasswordTitle bold>Reset Password</StyledResetPasswordTitle>
        <TextComponent>To be able to login again</TextComponent>
      </StyledWelcomeSection>
      <StyledResetPasswordForm>
        <ControlledInput
          name="password"
          control={control}
          secureTextEntry
          placeholder="Password"
        />
        <ControlledInput
          name="confirmPassword"
          control={control}
          secureTextEntry
          placeholder="Confirm Password"
        />
        <Button
          style={buttonStyle}
          type={ButtonTypes.primary}
          onPress={handleSubmit(onResetPassword)}
          title="Reset Password"
        />
      </StyledResetPasswordForm>
      <View style={{ flex: 2 }} />
    </StyledResetPasswordContainer>
  );
}
