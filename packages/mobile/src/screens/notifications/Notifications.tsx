import {
  ReactElement, Suspense, useMemo, useRef, useState,
} from 'react';
import { ImageViewer, NotificationComponent } from '@patrollar/mobile-components';
import { INotification, useNotifications } from '@patrollar/shared';
import { ActivityIndicator, FlatList } from 'react-native';

function Notifications(): ReactElement {
  const [notifications, loadMoreData] = useNotifications();
  const [currentIndex, setIndex] = useState<number>(NaN);
  const urls = useMemo(() => notifications.map(({ url: uri }) => ({ uri })), [notifications]);
  const fetching = useRef(false);

  function onNotificationPress(_: INotification, index: number): void {
    return setIndex(index);
  }

  function onImageClose(): void {
    return setIndex(NaN);
  }

  async function onImageChange(index: number): Promise<void> {
    const allSize = notifications.length;
    if (!Number.isFinite(index)) {
      return;
    }

    if (allSize - index <= 5 && !fetching.current) {
      fetching.current = true;
      await loadMoreData();
      fetching.current = false;
    }
  }

  function renderNotification({ item: notification, index }: { item: INotification, index: number }): ReactElement {
    return (
      <NotificationComponent
        notification={notification}
        index={index}
        onNotificationPress={onNotificationPress}
      />
    );
  }

  function keyExtractor({ id }: INotification): string {
    return id;
  }

  function getItemLayout(
    _data: INotification[] | null | undefined,
    index: number,
  ): { length: number; offset: number; index: number } {
    return { length: 100, offset: 100 * index, index };
  }

  return (
    <>
      <FlatList
        numColumns={1}
        data={notifications}
        renderItem={renderNotification}
        keyExtractor={keyExtractor}
        onEndReached={loadMoreData}
        getItemLayout={getItemLayout}
        initialNumToRender={20}
        onEndReachedThreshold={1}
      />
      <ImageViewer
        urls={urls}
        index={currentIndex}
        visible={Number.isFinite(currentIndex)}
        onClose={onImageClose}
        onChange={onImageChange}
      />
    </>
  );
}

export default function SuspendedNotifications(): ReactElement {
  return (
    <Suspense fallback={<ActivityIndicator style={{ alignSelf: 'center', flex: 1 }} size="large" />}>
      <Notifications />
    </Suspense>
  );
}
