import styled from 'styled-components';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View } from 'react-native';
import { TextComponent } from '@patrollar/mobile-components';

const StyledEmailConfirmationContainer = styled(SafeAreaView)`
  flex: 1;
  padding: 30px;
`;

const StyledTitle = styled(TextComponent)`
  font-size: 36px;
`;

const StyledContentContainer = styled(View)`
  flex: 2;
  justify-content: space-evenly;
`;

const StyledFooterContainer = styled(View)`
  flex: 2;
  justify-content: flex-end;
`;

export {
  StyledEmailConfirmationContainer, StyledTitle, StyledContentContainer, StyledFooterContainer,
};
