import type { ReactElement } from 'react';
import { Alert, View } from 'react-native';
import LottieView from 'lottie-react-native';
import { EmailAnimation } from '@patrollar/assets/animations';
import {
  Button, ButtonTypes, LinkText, TextComponent,
} from '@patrollar/mobile-components';
import { useNavigation } from '@react-navigation/native';
import { Routes } from '@patrollar/shared';
import { openInbox } from 'react-native-email-link';
import { buttonStyle } from '../register/Register.styled';
import {
  StyledEmailConfirmationContainer, StyledTitle, StyledContentContainer, StyledFooterContainer,
} from './EmailConfirmation.styled';
import type { NavigateProps } from '../../navigator/NavigatorProps';

export default function EmailConfirmation(): ReactElement {
  const { reset } = useNavigation<NavigateProps>();

  function onSkipPress(): void {
    return reset({
      index: 0,
      routes: [{ name: Routes.login }],
    });
  }

  function onRetryPress(): void {
    return Alert.alert('Not yet implemented.');
  }

  async function onOpenEmailPress(): Promise<void> {
    try {
      await openInbox();
    } catch (e) {
      Alert.alert('You don\'t have any Email app installed.');
    }
  }

  return (
    <StyledEmailConfirmationContainer>
      <View style={{ flex: 1 }}>
        <LottieView
          source={EmailAnimation as string}
          autoPlay
          loop={false}
        />
      </View>
      <StyledContentContainer>
        <View>
          <StyledTitle bold>Check your email</StyledTitle>
          <TextComponent>
            We have sent further instructions to your email address.
          </TextComponent>
        </View>
        <Button
          style={buttonStyle}
          type={ButtonTypes.primary}
          onPress={onOpenEmailPress}
          title="Open email app"
        />
        <Button
          type={ButtonTypes.link}
          onPress={onSkipPress}
        >
          <TextComponent>Skip, I&apos;ll check later</TextComponent>
        </Button>
      </StyledContentContainer>
      <StyledFooterContainer>
        <LinkText
          onPress={onRetryPress}
          prefix="Did not receive the email? Check your spam filter, or"
          link="contact support"
        />
      </StyledFooterContainer>
    </StyledEmailConfirmationContainer>
  );
}
