import styled from 'styled-components';
import { View } from 'react-native';
import { VideoPlayerComponent } from '@patrollar/mobile-components';

const StyledDetectionsContainer = styled(View)``;

const StyledDetectionsListContainer = styled(View)``;

const StyledVideoPlayer = styled(VideoPlayerComponent)<{ height: number }>`
  height: ${({ height }) => height}px;
`;

export { StyledDetectionsContainer, StyledDetectionsListContainer, StyledVideoPlayer };
