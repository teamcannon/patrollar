import {
  useLayoutEffect, Suspense, ReactElement, useState, useMemo, useRef,
} from 'react';
import {
  DetectionItemComponent,
  NoIntrusionComponent,
  DatesFilterComponent, ImageViewer,
} from '@patrollar/mobile-components';
import {
  ActionSheetState,
  DateFilterState, IDetection, useDetections,
} from '@patrollar/shared';
import { Dimensions, FlatList, useWindowDimensions } from 'react-native';
import { useRecoilState, useSetRecoilState } from 'recoil';
import { useNavigation, useRoute } from '@react-navigation/native';
import ContentLoader, { Rect } from 'react-content-loader/native';
import type { IDateFilter } from '@patrollar/assets/utils';
import { StyledDetectionsContainer, StyledVideoPlayer } from './Detections.styled';
import DetectionsHeader from '../../components/detections-header/DetectionsHeader';
import type { DetectionsRouteProp, NavigateProps } from '../../navigator/NavigatorProps';
import VideoHelper from '../../utils/VideoHelper';

function DetectionContainerFallback(): ReactElement {
  const { height } = Dimensions.get('window');
  const rectangleSize = height * 0.25;

  return (
    <ContentLoader backgroundColor="lightgray">
      <Rect x={10} y={20} rx={5} width="70%" height="20" />
      <Rect x="74%" y={10} rx={5} width="25%" height="40" />
      <Rect x={10} y={60} rx={5} width={rectangleSize} height={rectangleSize} />
      <Rect x={rectangleSize + 20} y={60} rx={5} width={rectangleSize} height={rectangleSize} />
    </ContentLoader>
  );
}

function DetectionContainer({ id: cameraId }: { id: string }): ReactElement {
  const [selectedDate, setSelectedDate] = useRecoilState(DateFilterState);
  const setActionSheet = useSetRecoilState(ActionSheetState);
  const [detections, loadMoreData] = useDetections(cameraId);
  const [currentIndex, setIndex] = useState<number>(NaN);
  const urls = useMemo(() => detections.map(({ url: uri }) => ({ uri })), [detections]);
  const fetching = useRef(false);

  function onDateChange(date: IDateFilter): void {
    setSelectedDate(date);
    setActionSheet({ content: null });
  }

  function onPress(): void {
    setActionSheet({
      content: <DatesFilterComponent selectedDate={selectedDate} onDateSelect={onDateChange} />,
    });
  }

  function onDetectionPress(_: IDetection, index: number): void {
    return setIndex(index);
  }

  function onImageClose(): void {
    return setIndex(NaN);
  }

  async function onImageChange(index: number): Promise<void> {
    const allSize = detections.length;
    if (!Number.isFinite(index)) {
      return;
    }

    if (allSize - index <= 2 && !fetching.current) {
      fetching.current = true;
      await loadMoreData();
      fetching.current = false;
    }
  }

  function renderItem({ item, index }: { item: IDetection, index: number }): ReactElement {
    return (
      <DetectionItemComponent
        item={item}
        index={index}
        onDetectionPress={onDetectionPress}
      />
    );
  }

  function keyExtractor({ id: detectionId }: IDetection): string {
    return detectionId;
  }

  return (
    <>
      <DetectionsHeader text={`${selectedDate.name}'s detections`} onPress={onPress} />
      <FlatList
        ListEmptyComponent={<NoIntrusionComponent />}
        numColumns={1}
        data={detections}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        contentContainerStyle={{ flexGrow: 1 }}
        onEndReached={loadMoreData}
        initialNumToRender={5}
        onEndReachedThreshold={0.5}
        horizontal
      />
      <ImageViewer
        urls={urls}
        index={currentIndex}
        visible={Number.isFinite(currentIndex)}
        onClose={onImageClose}
        onChange={onImageChange}
      />
    </>
  );
}

export default function Detections(): ReactElement {
  const { params } = useRoute<DetectionsRouteProp>();
  const navigation = useNavigation<NavigateProps>();
  const { camera: { id, name, url } } = params;
  const { height } = useWindowDimensions();
  const posterUrl = VideoHelper.getPosterUrl(id);

  useLayoutEffect(() => navigation.setOptions({
    title: name,
  }), [navigation]);

  return (
    <StyledDetectionsContainer>
      <StyledVideoPlayer
        controls
        resizeMode="cover"
        height={height * 0.5}
        url={url}
        poster={posterUrl}
        isPlaying
      />
      <Suspense fallback={<DetectionContainerFallback />}>
        <DetectionContainer id={id} />
      </Suspense>
    </StyledDetectionsContainer>
  );
}
