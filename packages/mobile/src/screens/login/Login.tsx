import { ReactElement, useContext } from 'react';
import {
  LoginFormInputs,
  LoginFormSchema, PatrollarContext, Routes,
} from '@patrollar/shared';
import {
  Button, ButtonTypes, TextComponent,
} from '@patrollar/mobile-components';
import { useNavigation } from '@react-navigation/native';
import { Google } from '@patrollar/assets/icons';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import { Alert, Platform } from 'react-native';
import { AppLogo } from '@patrollar/assets/logos';
import {
  buttonStyle,
  StyledAlternativeSignupContainer,
  StyledForgotPasswordContainer,
  StyledHairlineLine,
  StyledInputsContainer,
  StyledLoginContainer,
  StyledCenteredContainer,
  StyledWelcomeMessage,
  StyledPrimaryText,
  StyledWelcomeSection,
} from './Login.styled';
import type { NavigateProps } from '../../navigator/NavigatorProps';
import ControlledInput from '../../components/inputs/ControlledInput';

function AlternativeSignupSection(): ReactElement {
  const { reset } = useNavigation<NavigateProps>();

  function onGooglePress(): void {
    return Alert.alert('Not yet implemented');
  }

  function onCreateAccountPress(): void {
    return reset({
      index: 0,
      routes: [{ name: Routes.register }],
    });
  }

  function getFooter(): ReactElement {
    return (
      <StyledCenteredContainer>
        <TextComponent>Don&apos;t have an account? </TextComponent>
        <Button type={ButtonTypes.link} onPress={onCreateAccountPress}>
          <StyledPrimaryText bold>
            Create one
          </StyledPrimaryText>
        </Button>
      </StyledCenteredContainer>
    );
  }

  return (
    <StyledAlternativeSignupContainer>
      <StyledCenteredContainer>
        <StyledHairlineLine />
        <TextComponent>Or</TextComponent>
        <StyledHairlineLine />
      </StyledCenteredContainer>
      <Button
        style={buttonStyle}
        type={ButtonTypes.icon}
        icon={<Google width={30} height={20} />}
        onPress={onGooglePress}
      >
        <TextComponent>Sign in with Google</TextComponent>
      </Button>
      {getFooter()}
    </StyledAlternativeSignupContainer>
  );
}

export default function Login(): ReactElement {
  const { navigate } = useNavigation<NavigateProps>();
  const { handleSubmit, control } = useForm<LoginFormInputs>({
    resolver: zodResolver(LoginFormSchema),
    mode: 'onBlur',
  });

  const { userService } = useContext(PatrollarContext);

  async function onLoginPress(data: LoginFormInputs): Promise<void> {
    const { email, password } = data;
    await userService.login(email, password);
  }

  function onForgotPasswordPress(): void {
    return navigate(Routes.forgotPassword);
  }

  function getForgotPasswordButton(): ReactElement {
    return (
      <StyledForgotPasswordContainer>
        <Button type={ButtonTypes.link} onPress={onForgotPasswordPress}>
          <TextComponent>Forgot Password?</TextComponent>
        </Button>
      </StyledForgotPasswordContainer>
    );
  }

  function getLoginForm(): ReactElement {
    return (
      <StyledInputsContainer>
        <ControlledInput
          name="email"
          control={control}
          keyboardType="email-address"
          placeholder="Email"
        />
        <ControlledInput
          name="password"
          control={control}
          secureTextEntry
          placeholder="Password"
        />
        {getForgotPasswordButton()}
        <Button style={buttonStyle} type={ButtonTypes.primary} onPress={handleSubmit(onLoginPress)} title="Sign In" />
      </StyledInputsContainer>
    );
  }

  return (
    <StyledLoginContainer behavior={Platform.OS === 'ios' ? 'padding' : 'height'} enabled={false}>
      <StyledWelcomeSection>
        <AppLogo style={{ height: 50 }} />
        <StyledWelcomeMessage bold>Welcome Back</StyledWelcomeMessage>
        <TextComponent>Sign in to secure your home</TextComponent>
      </StyledWelcomeSection>
      {getLoginForm()}
      <AlternativeSignupSection />
    </StyledLoginContainer>
  );
}
