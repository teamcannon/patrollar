import styled, { css } from 'styled-components';
import { TextComponent } from '@patrollar/mobile-components';
import {
  Dimensions, KeyboardAvoidingView, StyleSheet, View,
} from 'react-native';

const StyledLoginContainer = styled(KeyboardAvoidingView)`
  padding: 30px;
  min-height: ${Dimensions.get('screen').height}px;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.background};
`;

const StyledWelcomeMessage = styled(TextComponent)`
  font-size: 36px;
`;

const StyledWelcomeSection = styled(View)`
  flex: 2;
  justify-content: center;
`;

const buttonStyle = css`
  padding: 20px;
  border-radius: 60px;
`;

const StyledHairlineLine = styled(View)`
  width: 15px;
  border-bottom-color: black;
  border-bottom-width: ${StyleSheet.hairlineWidth}px;
  margin: 0 10px;
`;

const StyledInputsContainer = styled(View)`
  flex: 4;
  justify-content: space-evenly;
`;

const StyledForgotPasswordContainer = styled(View)`
  align-self: flex-end;
  margin-bottom: 10px;
`;

const StyledAlternativeSignupContainer = styled(View)`
  flex: 4;
  justify-content: space-evenly;
`;

const StyledCenteredContainer = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const StyledPrimaryText = styled(TextComponent)`
  color: #4CBDAD
`;

export {
  StyledLoginContainer,
  StyledWelcomeMessage,
  StyledWelcomeSection,
  buttonStyle,
  StyledHairlineLine,
  StyledInputsContainer,
  StyledForgotPasswordContainer,
  StyledAlternativeSignupContainer,
  StyledCenteredContainer,
  StyledPrimaryText,
};
