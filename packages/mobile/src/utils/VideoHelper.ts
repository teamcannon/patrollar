import FileSystem from 'react-native-fs';

interface VideoHelperReturn {
  getPosterUrl: (cameraId: string, format?: string) => string
}

function VideoHelper(): VideoHelperReturn {
  function getPosterUrl(cameraId: string, format = 'jpg'): string {
    return `file://${FileSystem.DocumentDirectoryPath}/${cameraId}.${format}`;
  }

  return {
    getPosterUrl,
  };
}

export default VideoHelper();
