import {
  AvatarComponent, Separator, Button, ButtonTypes,
} from '@patrollar/mobile-components';
import {
  memo, ReactElement, useContext, useEffect, useMemo, useRef,
} from 'react';
import { CamerasState, UserState, PatrollarContext } from '@patrollar/shared';
import {
  Animated, Easing, useWindowDimensions, View, Pressable,
} from 'react-native';
import { LockAnimation } from '@patrollar/assets/animations';
import LottieView from 'lottie-react-native';
import { useRecoilState, useRecoilValue } from 'recoil';
import {
  StyledDisplayName,
  StyledEmail,
  StyledProfileSettingsContainer,
} from './ProfileSettings.styled';

function LockComponent(): ReactElement {
  const [cameras, setCameras] = useRecoilState(CamerasState);
  const isAllArmed = useMemo(() => cameras.every(({ isArmed }) => isArmed), [cameras]);
  const progress = useRef(new Animated.Value(isAllArmed ? 0.6 : 0.15));
  const { cameraService } = useContext(PatrollarContext);

  useEffect(() => {
    Animated.timing(progress.current, {
      toValue: isAllArmed ? 0.6 : 0.15,
      duration: 1500,
      easing: Easing.linear,
      useNativeDriver: false,
    }).start();
  }, [isAllArmed]);

  async function onLockPress(): Promise<void> {
    // TODO handle errors ?
    await cameraService.arm();
    const newCameras = cameras.map((camera) => ({ ...camera, isArmed: !isAllArmed }));
    return setCameras(newCameras);
  }

  return (
    <Pressable
      style={{ height: 250, width: 250, alignSelf: 'center' }}
      onPress={onLockPress}
    >
      <LottieView
        source={LockAnimation as string}
        progress={progress.current}
        loop={false}
        autoPlay={false}
      />
    </Pressable>
  );
}

function ProfileSettings(): ReactElement {
  const photoURL = 'https://www.signivis.com/img/custom/avatars/member-avatar-01.png';
  const { email, phoneNumber } = useRecoilValue(UserState);
  const { width } = useWindowDimensions();
  const { userService } = useContext(PatrollarContext);

  async function onLogoutPress(): Promise<void> {
    return userService.logout();
  }

  return (
    <View>
      <StyledProfileSettingsContainer>
        <AvatarComponent url={photoURL} width={100} />
        <StyledDisplayName>{phoneNumber}</StyledDisplayName>
        <StyledEmail>{email}</StyledEmail>
        <Separator size={width * 0.9} />
      </StyledProfileSettingsContainer>
      <Button type={ButtonTypes.primary} onPress={onLogoutPress} title="Logout" />
      <LockComponent />
    </View>
  );
}

export default memo(ProfileSettings);
