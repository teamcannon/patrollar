import { TextComponent } from '@patrollar/mobile-components';
import styled from 'styled-components';
import { View } from 'react-native';

const StyledProfileSettingsContainer = styled(View)`
  align-items: center;
  margin-top: 10px;
`;

const StyledDisplayName = styled(TextComponent)`
  font-size: 18px;
  font-weight: 600;
`;

const StyledEmail = styled(TextComponent)`
  font-size: 18px;
  font-weight: 600;
  opacity: 0.5;
`;

export { StyledProfileSettingsContainer, StyledDisplayName, StyledEmail };
