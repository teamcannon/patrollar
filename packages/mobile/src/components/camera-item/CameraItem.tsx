import type { ReactElement } from 'react';
import type { ICamera } from '@patrollar/shared';
import {
  StyledCameraItemPressable,
} from './CameraItem.styled';
import CameraItemOverlay from './overlay/CameraItemOverlay';
import VideoPlayerContainer from './video-player/VideoPlayerContainer';
import CameraInfo from './info/CameraInfo';

interface CameraItemProps {
  camera: ICamera;
  onCameraPress: (camera: ICamera) => void;
  onCameraPlayPress: (camera: ICamera) => void;
}

export default function CameraItem(props: CameraItemProps): ReactElement {
  const { camera, onCameraPress, onCameraPlayPress } = props;
  const { id, url } = camera;

  function onPress(): void {
    return onCameraPress(camera);
  }

  function onPlayPress(): void {
    return onCameraPlayPress(camera);
  }

  return (
    <StyledCameraItemPressable onPress={onPress}>
      <VideoPlayerContainer id={id} url={url} />
      <CameraItemOverlay id={id} onPlayPress={onPlayPress} />
      <CameraInfo camera={camera} />
    </StyledCameraItemPressable>
  );
}
