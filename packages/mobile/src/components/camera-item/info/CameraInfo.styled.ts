import styled from 'styled-components/native';
import { TextComponent } from '@patrollar/mobile-components';
import { Arrow } from '@patrollar/assets/icons';
import { AppLogo as Logo } from '@patrollar/assets/logos';

const CameraItemDetails = styled.View`
  justify-content: space-between;
  flex-direction: row;
  padding: 10px;
`;

const CameraItemText = styled(TextComponent)`
  font-weight: 500;
  font-size: 18px;
  color: ${({ theme }) => theme.colors.fontColor};
`;

const CameraItemArrow = styled(Arrow)`
  width: 25px;
  height: 25px;
  color: ${({ theme }) => theme.colors.fontColor};
`;

const CameraItemContainer = styled.View`
  align-items: center;
  flex-direction: row;
  flex: 1;
  padding: 0 5px;
  min-height: 50px;
`;

const CameraItemAlerts = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
`;

const CameraItemSiren = styled.View`
  width: 30px;
  height: 30px;
  justify-content: center;
  align-items: center;
`;

const CameraItemIntrusions = styled(TextComponent)`
  font-weight: normal;
  font-size: 14px;
  opacity: 0.7;
  color: ${({ theme }) => theme.colors.fontColor};
`;

const CameraItemDetection = styled.View`
  align-items: center;
  flex: 1;
`;

const AppLogo = styled(Logo)`
  align-self: center;
  aspect-ratio: ${() => 0.65};
`;

const CameraDetectorDot = styled.View<{ color: string }>`
  width: 15px;
  height: 15px;
  border-radius: 7.5px;
  background-color: ${({ color }) => color};
`;

export {
  CameraItemDetails,
  CameraItemText,
  CameraItemArrow,
  CameraItemAlerts,
  CameraItemSiren,
  CameraItemIntrusions,
  CameraItemContainer,
  CameraItemDetection,
  AppLogo,
  CameraDetectorDot,
};
