import type { ICamera } from '@patrollar/shared';
import type { ReactElement } from 'react';
import { useWindowDimensions, View } from 'react-native';
import { Suspense, useContext } from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';
import {
  CamerasDetectorSelector,
  CamerasState,
  PatrollarContext,
  UnreadCameraNotificationsCountState,
} from '@patrollar/shared';
import { Dot, Separator, SwitchComponent } from '@patrollar/mobile-components';
import ContentLoader, { Circle, Rect } from 'react-content-loader/native';
import LottieView from 'lottie-react-native';
import { ErrorAnimation } from '@patrollar/assets/animations';
import * as styled from './CameraInfo.styled';

function SwitchContainer({ camera }: { camera: ICamera }): ReactElement {
  const [cameras, setCameras] = useRecoilState(CamerasState);
  const { cameraService } = useContext(PatrollarContext);

  async function onArmPress(): Promise<void> {
    const newCamera = {
      ...camera,
      isArmed: !camera.isArmed,
    };
    const newCameras = cameras.map((value) => (value.id === camera.id ? newCamera : value));
    setCameras(newCameras);
    await cameraService.arm(camera.id);
  }

  return <SwitchComponent onValueChange={onArmPress} value={camera.isArmed} />;
}

function DetectionCounterContainerFallback(): ReactElement {
  return (
    <View style={{ flex: 1 }}>
      <ContentLoader backgroundColor="lightgray">
        <Circle cx="50%" cy="40%" r="10" />
        <Rect x="20%" y="70%" rx="5" width="65" height="20%" />
      </ContentLoader>
    </View>
  );
}

function DetectionCounterContainer({ id }: { id: string }): ReactElement {
  const notificationCount = useRecoilValue(UnreadCameraNotificationsCountState(id));
  const message = `${notificationCount || 'No'} intrusions`;

  return (
    <styled.CameraItemAlerts>
      <styled.CameraItemSiren>
        {notificationCount > 0 && <LottieView source={ErrorAnimation as string} loop autoPlay />}
        {!notificationCount && <styled.AppLogo />}
      </styled.CameraItemSiren>
      <styled.CameraItemIntrusions>{message}</styled.CameraItemIntrusions>
    </styled.CameraItemAlerts>
  );
}

function ArmCameraContainer({ camera }: { camera: ICamera }): ReactElement {
  return (
    <styled.CameraItemDetection>
      <SwitchContainer camera={camera} />
      <styled.CameraItemIntrusions>Armed</styled.CameraItemIntrusions>
    </styled.CameraItemDetection>
  );
}

function DetectorStatusContainer({ id }: { id: string }): ReactElement {
  const isDetectorAlive = useRecoilValue(CamerasDetectorSelector(id));
  const color = isDetectorAlive ? 'green' : 'red';

  return (
    <styled.CameraItemAlerts>
      <styled.CameraItemSiren>
        <styled.CameraDetectorDot color={color} />
        <Dot size={40} delay={10} color={color} repeat />
      </styled.CameraItemSiren>
      <styled.CameraItemIntrusions>Detector</styled.CameraItemIntrusions>
    </styled.CameraItemAlerts>
  );
}

export default function CameraInfo({ camera }: { camera: ICamera }): ReactElement {
  const { name, id } = camera;
  const { width } = useWindowDimensions();

  return (
    <>
      <styled.CameraItemDetails>
        <styled.CameraItemText>{name}</styled.CameraItemText>
        <styled.CameraItemArrow />
      </styled.CameraItemDetails>
      <Separator size={width * 0.8} />
      <styled.CameraItemContainer>
        <Suspense fallback={<DetectionCounterContainerFallback />}>
          <DetectionCounterContainer id={id} />
        </Suspense>
        <Separator size={30} horizontal />
        <Suspense fallback={<DetectionCounterContainerFallback />}>
          <DetectorStatusContainer id={id} />
        </Suspense>
        <Separator size={30} horizontal />
        <ArmCameraContainer camera={camera} />
      </styled.CameraItemContainer>
    </>
  );
}
