import styled from 'styled-components';
import { Pressable, Dimensions } from 'react-native';

const StyledCameraItemPressable = styled(Pressable)`
  height: auto;
  width: ${Dimensions.get('window').width * 0.88}px;
  align-self: center;
  border-radius: 12px;
  background-color: ${({ theme }) => theme.colors.cameraItemBackground};
  border: 1px solid ${({ theme }) => theme.colors.cameraItemBorder};
  margin: 10px 0;
`;

export {
  // eslint-disable-next-line import/prefer-default-export
  StyledCameraItemPressable,
};
