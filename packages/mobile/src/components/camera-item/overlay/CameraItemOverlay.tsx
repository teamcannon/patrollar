import type { ReactElement } from 'react';
import { useRecoilValue } from 'recoil';
import { VisibleCameraState } from '@patrollar/shared';
import { Play } from '@patrollar/assets/icons';
import { StyledPlayButton } from './CameraItemOverlay.styled';

interface CameraItemOverlayProps {
  id: string;
  onPlayPress: () => void;
}

export default function CameraItemOverlay(
  { id, onPlayPress }: CameraItemOverlayProps,
): ReactElement {
  const visibleCamera = useRecoilValue(VisibleCameraState);
  const isPlaying = visibleCamera.id === id;

  if (isPlaying) {
    return <></>;
  }
  return (
    <StyledPlayButton onPress={onPlayPress}>
      <Play color="white" width={48} height={48} />
    </StyledPlayButton>
  );
}
