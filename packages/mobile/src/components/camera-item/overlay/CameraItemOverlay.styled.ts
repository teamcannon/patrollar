import styled from 'styled-components';
import { Pressable } from 'react-native';

const StyledPlayButton = styled(Pressable)`
  position: absolute;
  align-self: center;
  top: 25%;
  border-radius: 25px;
  background-color: rgba(0, 0, 0, 0.25);
`;

// eslint-disable-next-line import/prefer-default-export
export { StyledPlayButton };
