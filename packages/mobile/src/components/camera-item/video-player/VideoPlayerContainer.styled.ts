import styled from 'styled-components';
import { View, Image } from 'react-native';

const StyledPlaceholderContainer = styled(View)`
  width: 100%;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  aspect-ratio: ${() => 16 / 9};
`;

const StyledImagePlaceholder = styled(Image)`
  width: 100%;
  border-top-right-radius: 10px;
  border-top-left-radius: 10px;
  aspect-ratio: ${() => 16 / 9};
`;

export { StyledPlaceholderContainer, StyledImagePlaceholder };
