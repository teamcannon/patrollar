import { ReactElement, useRef } from 'react';
import { useRecoilValue } from 'recoil';
import { VisibleCameraState } from '@patrollar/shared';
import ViewShot, { captureRef } from 'react-native-view-shot';
import FileSystem from 'react-native-fs';
import { VideoPlayerComponent } from '@patrollar/mobile-components';
import { css } from 'styled-components';
import VideoHelper from '../../../utils/VideoHelper';
import { StyledImagePlaceholder, StyledPlaceholderContainer } from './VideoPlayerContainer.styled';

const videoStyle = css`
  border-radius: 10px;
  aspect-ratio: ${() => 16 / 9};
`;

export default function VideoPlayerContainer(
  { id, url }: { id: string, url: string },
): ReactElement {
  const viewShot = useRef(null);
  const visibleCamera = useRecoilValue(VisibleCameraState);
  const isPlaying = visibleCamera.id === id;
  const posterUrl = VideoHelper.getPosterUrl(id);

  async function onReadyForDisplay(): Promise<void> {
    const screenshot = await captureRef(viewShot, { format: 'jpg', quality: 0.5, result: 'base64' });
    if (await FileSystem.exists(posterUrl)) {
      await FileSystem.unlink(posterUrl);
    }
    await FileSystem.writeFile(posterUrl, screenshot, 'base64');
  }

  if (!isPlaying) {
    return (
      <StyledPlaceholderContainer>
        <StyledImagePlaceholder source={{ uri: posterUrl, cache: 'reload' }} />
      </StyledPlaceholderContainer>
    );
  }

  return (
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    <ViewShot ref={viewShot}>
      <VideoPlayerComponent
        resizeMode="cover"
        url={url}
        style={videoStyle}
        controls={false}
        onReadyForDisplay={onReadyForDisplay}
        isPlaying={isPlaying}
        poster={posterUrl}
      />
    </ViewShot>
  );
}
