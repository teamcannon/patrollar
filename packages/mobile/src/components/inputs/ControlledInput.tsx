import type { ReactElement } from 'react';
import { FieldError, useController, UseControllerProps } from 'react-hook-form';
import { DefaultInputProps, DefaultInput, WithStyle } from '@patrollar/mobile-components';

export interface ControlledInputProps<T> extends DefaultInputProps, UseControllerProps<T> {}

export default function ControlledInput<T extends Record<string, string>>(
  props: WithStyle<ControlledInputProps<T>>,
): ReactElement {
  const {
    keyboardType, placeholder, secureTextEntry, name, control, defaultValue, style, containerStyle,
  } = props;
  const controller = useController({ name, control, defaultValue });

  const {
    field: {
      ref, onBlur, onChange, value,
    },
    fieldState: { error, invalid },
  } = controller;

  return (
    <DefaultInput
      onChangeText={onChange}
      placeholder={placeholder}
      keyboardType={keyboardType}
      onBlur={onBlur}
      value={value}
      invalid={invalid}
      errorMessage={(error as FieldError)?.message}
      secureTextEntry={secureTextEntry}
      style={style}
      containerStyle={containerStyle}
      ref={ref}
    />
  );
}
