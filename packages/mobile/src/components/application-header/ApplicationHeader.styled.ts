import styled, { css } from 'styled-components';
import { View } from 'react-native';

const StyledApplicationHeaderContainer = styled(View)`
  flex-direction: row;
  width: 80px;
  justify-content: space-between;
  align-items: center;
`;

const avatarStyle = css`
  margin-left: 10px;
`;

const unreadNotificationStyle = css`
  position: absolute;
  align-self: flex-end;
`;

export { StyledApplicationHeaderContainer, avatarStyle, unreadNotificationStyle };
