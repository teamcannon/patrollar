import { ActionSheetState, Routes } from '@patrollar/shared';
import { useSetRecoilState } from 'recoil';
import { Pressable } from 'react-native';
import { memo, ReactElement } from 'react';
import { AvatarComponent, UnreadNotificationComponent } from '@patrollar/mobile-components';
import { Bell } from '@patrollar/assets/icons';
import ProfileSettings from '../profile-settings/ProfileSettings';
import {
  avatarStyle,
  StyledApplicationHeaderContainer,
  unreadNotificationStyle,
} from './ApplicationHeader.styled';
import { navigate } from '../../navigator/NavigationRef';

// TODO check re-renders
function ApplicationHeader(): ReactElement {
  const photoURL = 'https://www.signivis.com/img/custom/avatars/member-avatar-01.png';
  const setActionSheet = useSetRecoilState(ActionSheetState);

  function onNotificationPress(): void {
    return navigate(Routes.notifications);
  }

  function onAvatarPress(): void {
    setActionSheet({ content: <ProfileSettings /> });
  }

  return (
    <StyledApplicationHeaderContainer>
      <Pressable hitSlop={10} onPress={onNotificationPress}>
        <UnreadNotificationComponent width={5} style={unreadNotificationStyle} />
        <Bell width={24} height={24} color="black" />
      </Pressable>
      <AvatarComponent
        url={photoURL}
        width={30}
        onPress={onAvatarPress}
        style={avatarStyle}
      />
    </StyledApplicationHeaderContainer>
  );
}

export default memo(ApplicationHeader);
