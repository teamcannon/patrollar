import { useRecoilValue } from 'recoil';
import { ActionSheetState } from '@patrollar/shared';
import { ReactElement, useEffect, useRef } from 'react';
import { ActionSheetComponent } from '@patrollar/mobile-components';
import type BottomSheet from '@gorhom/bottom-sheet';

export default function ActionSheetContainer(): ReactElement {
  const actionSheetRef = useRef<BottomSheet>(null);
  const actionSheetState = useRecoilValue(ActionSheetState);
  const { content } = actionSheetState;

  useEffect(() => {
    // TODO maybe remove timeout after this is fixed https://github.com/gorhom/react-native-bottom-sheet/issues/566
    setTimeout(() => {
      if (!content) {
        actionSheetRef.current?.collapse();
      } else {
        actionSheetRef.current?.expand();
      }
    }, 50);
  }, [actionSheetState]);

  return <ActionSheetComponent ref={actionSheetRef}>{content}</ActionSheetComponent>;
}
