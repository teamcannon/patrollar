import { Button, ButtonTypes } from '@patrollar/mobile-components';
import type { ReactElement } from 'react';
import { DateFilterSectionContainer, StyledDetectionsFilterMessage } from './DetectionsHeader.styled';

interface DetectionsHeaderProps {
  text: string;
  onPress: () => void;
}

export default function DetectionsHeader({ text, onPress }: DetectionsHeaderProps): ReactElement {
  return (
    <DateFilterSectionContainer>
      <StyledDetectionsFilterMessage>{text}</StyledDetectionsFilterMessage>
      <Button type={ButtonTypes.primary} onPress={onPress} title="Change" />
    </DateFilterSectionContainer>
  );
}
