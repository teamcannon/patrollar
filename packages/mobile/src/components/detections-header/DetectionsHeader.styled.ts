import styled from 'styled-components';
import { View } from 'react-native';
import { TextComponent } from '@patrollar/mobile-components';

const DateFilterSectionContainer = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 10px;
`;

const StyledDetectionsFilterMessage = styled(TextComponent)`
  font-size: 18px;
`;

export { DateFilterSectionContainer, StyledDetectionsFilterMessage };
