import type { IStorageService } from '@patrollar/shared';
import { singleton } from 'tsyringe';
import { MMKVLoader } from 'react-native-mmkv-storage';

@singleton()
export default class SecureStorageService implements IStorageService {
  private userStorage = new MMKVLoader()
    .withEncryption()
    .withInstanceID('userdata')
    .initialize();

  setString(key: string, value: string): boolean | null | undefined {
    return this.userStorage.setString(key, value);
  }

  getString(key: string): string | null | undefined {
    return this.userStorage.getString(key);
  }

  remove(key: string): boolean | null | undefined {
    return this.userStorage.removeItem(key);
  }
}
