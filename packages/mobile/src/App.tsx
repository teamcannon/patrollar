import {
  ReactElement, Suspense, useContext, useEffect, useState,
} from 'react';
import { ActivityIndicator } from 'react-native';
import { ThemeProvider } from 'styled-components';
import { Theme } from '@patrollar/assets/theme';
import { MutableSnapshot, RecoilRoot } from 'recoil';
import {
  ApplicationType,
  IDevice,
  logger,
  PatrollarContext,
  SimpleContext,
  SimplePatrollarContext,
  StartupService,
  UserState,
} from '@patrollar/shared';
import messaging from '@react-native-firebase/messaging';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { pipe, subscribe } from 'wonka';
import type { IUser } from '@patrollar/shared/src/interfaces/IUser';
import usePromise from 'react-promise-suspense';
import { init as sentryInit } from '@sentry/react-native';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import Navigator from './navigator/Navigator';
import ActionSheetContainer from './components/action-sheet/ActionSheetContainer';
import SecureStorageService from './services/SecureStorageService';

sentryInit({
  dsn: 'https://1bd76fef13ad4a509e027bbb24a70a2a@o1002651.ingest.sentry.io/5962889',
});

function NavigatorWrapper(): ReactElement {
  const { deviceService, userService } = useContext(PatrollarContext);

  const userResult: IUser | null = usePromise(
    async () => {
      try {
        if (userService.isAuthenticated()) {
          return await userService.me();
        }
      } catch (e) {
        return null;
      }

      return null;
    },
    [],
  );

  const [user, setUser] = useState<IUser | null>(userResult);
  const isAuthenticated = !!user;

  useEffect(() => {
    const { unsubscribe } = pipe(
      userService.authenticationSubject.source,
      subscribe((newUser) => setUser(newUser)),
    );

    return unsubscribe;
  }, []);

  useEffect(() => {
    (async function updateFCMToken(): Promise<IDevice | null> {
      const fcmToken = await messaging().getToken();
      if (!isAuthenticated) {
        return null;
      }
      return deviceService.save({
        fcmToken,
        application: ApplicationType.MOBILE,
      });
    }());

    return messaging().onTokenRefresh((token) => deviceService.save({
      fcmToken: token,
      application: ApplicationType.MOBILE,
    }));
  }, [isAuthenticated]);

  function initializeState({ set }: MutableSnapshot): void {
    if (isAuthenticated) {
      set(UserState, user);
    }
  }

  if (isAuthenticated) {
    return (
      <RecoilRoot initializeState={initializeState}>
        <Navigator isAuthenticated />
        <ActionSheetContainer />
      </RecoilRoot>
    );
  }

  return <Navigator isAuthenticated={false} />;
}

export default function App(): ReactElement {
  const patrollarContext = SimpleContext.getContext(SimplePatrollarContext);

  useEffect(() => {
    // TODO this should be initialized on index.ts and the caching problem should be solved
    StartupService.initialize(SecureStorageService);
    (async function requestPermission(): Promise<void> {
      const authStatus = await messaging().requestPermission();
      const enabled = authStatus === messaging.AuthorizationStatus.AUTHORIZED
        || authStatus === messaging.AuthorizationStatus.PROVISIONAL;
      const initialNotification = await messaging().getInitialNotification();
      logger.info('initial notification', JSON.stringify(initialNotification, null, 2));

      if (enabled) {
        // console.log('Authorization status:', authStatus);
      }
    }());
  }, []);

  return (
    <PatrollarContext.Provider value={patrollarContext}>
      <ThemeProvider theme={Theme}>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <SafeAreaProvider>
            <Suspense fallback={<ActivityIndicator style={{ alignSelf: 'center', flex: 1 }} size="large" />}>
              <NavigatorWrapper />
            </Suspense>
          </SafeAreaProvider>
        </GestureHandlerRootView>
      </ThemeProvider>
    </PatrollarContext.Provider>
  );
}
