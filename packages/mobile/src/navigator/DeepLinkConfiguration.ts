import type { LinkingOptions } from '@react-navigation/native';
import { Routes } from '@patrollar/shared';
import type { RootStackParamList } from './NavigatorProps';

export default {
  prefixes: ['patrollar://'],
  config: {
    screens: {
      [Routes.login]: Routes.login,
      [Routes.resetPassword]: Routes.resetPassword,
    },
  },
} as LinkingOptions<RootStackParamList>;
