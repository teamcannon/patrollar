import { ICamera, Routes } from '@patrollar/shared';
import type { NativeStackNavigationOptions } from '@react-navigation/native-stack';
import type { RouteProp } from '@react-navigation/native';
import type { StackNavigationProp } from '@react-navigation/stack';
import type { ComponentType } from 'react';

export type RootStackParamList = {
  [Routes.login]: undefined;
  [Routes.register]: undefined;
  [Routes.forgotPassword]: undefined;
  [Routes.emailConfirmation]: undefined;
  [Routes.resetPassword]: { email: string };
  [Routes.cameras]: undefined;
  [Routes.notifications]: undefined;
  [Routes.detections]: { camera: ICamera };
};

export type IScreenOptions =
  Record<Routes, { component: ComponentType, options: NativeStackNavigationOptions }>;

export type NavigateProps = StackNavigationProp<RootStackParamList>;

export type DetectionsRouteProp = RouteProp<RootStackParamList, Routes.detections>;

export type ResetPasswordRouteProp = RouteProp<RootStackParamList, Routes.resetPassword>;
