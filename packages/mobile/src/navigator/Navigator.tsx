import { memo, ReactElement } from 'react';
import { createNativeStackNavigator, NativeStackNavigationOptions } from '@react-navigation/native-stack';
import { Routes } from '@patrollar/shared';
import { NavigationContainer } from '@react-navigation/native';
import { Platform } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';
import Login from '../screens/login/Login';
import Cameras from '../screens/cameras/Cameras';
import Detections from '../screens/detections/Detections';
import Notifications from '../screens/notifications/Notifications';
import ApplicationHeader from '../components/application-header/ApplicationHeader';
import { navigationRef } from './NavigationRef';
import type { IScreenOptions, RootStackParamList } from './NavigatorProps';
import Register from '../screens/register/Register';
import ForgotPassword from '../screens/forgot-password/ForgotPassword';
import EmailConfirmation from '../screens/email-confirmation/EmailConfirmation';
import LinkingOptions from './DeepLinkConfiguration';
import ResetPassword from '../screens/reset-password/ResetPassword';

const ApplicationStack = createNativeStackNavigator<RootStackParamList>();

const fullscreenOptions: NativeStackNavigationOptions = {
  statusBarStyle: 'dark',
  headerShadowVisible: false,
  headerTintColor: 'black',
  headerTitle: '',
  headerTransparent: true,
  headerStyle: { backgroundColor: 'transparent' },
  headerRight: () => null,
};

const ScreenConfigs: IScreenOptions = {
  [Routes.login]: {
    component: Login,
    options: fullscreenOptions,
  },
  [Routes.register]: {
    component: Register,
    options: fullscreenOptions,
  },
  [Routes.forgotPassword]: {
    component: ForgotPassword,
    options: fullscreenOptions,
  },
  [Routes.emailConfirmation]: {
    component: EmailConfirmation,
    options: fullscreenOptions,
  },
  [Routes.resetPassword]: {
    component: ResetPassword,
    options: fullscreenOptions,
  },
  [Routes.cameras]: {
    component: Cameras,
    options: {
      title: 'Cameras',
    },
  },
  [Routes.detections]: {
    component: Detections,
    options: {},
  },
  [Routes.notifications]: {
    component: Notifications,
    options: {
      title: 'Notifications',
    },
  },
};

const screenOptions: NativeStackNavigationOptions = {
  headerRight() {
    return <ApplicationHeader />;
  },
  headerTitleStyle: { fontFamily: 'Avenir', fontWeight: '600' },
  headerBackTitleStyle: { fontFamily: 'Avenir' },
  statusBarStyle: 'dark',
  animation: Platform.OS === 'android' ? 'none' : 'default',
  // stackAnimation: 'slide_from_right',
  headerTintColor: '#222121',
  orientation: 'portrait',
};

function onNavigationReady(): Promise<void> {
  return RNBootSplash.hide({ fade: true });
}

function Navigator({ isAuthenticated }: { isAuthenticated: boolean }): ReactElement {
  return (
    <NavigationContainer linking={LinkingOptions} ref={navigationRef} onReady={onNavigationReady}>
      <ApplicationStack.Navigator
        initialRouteName={isAuthenticated ? Routes.cameras : Routes.login}
        screenOptions={screenOptions}
      >
        {Object.entries(ScreenConfigs).map(([name, { options, component }]) => (
          <ApplicationStack.Screen
            key={name}
            name={name as Routes}
            component={component}
            options={options}
          />
        ))}
      </ApplicationStack.Navigator>
    </NavigationContainer>
  );
}

export default memo(Navigator);
