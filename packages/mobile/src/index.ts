import '@abraham/reflection';
import 'react-native-gesture-handler';
import { AppRegistry } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { logger } from '@patrollar/shared';
import Animated, { Extrapolation } from 'react-native-reanimated';
import Application from './App';

try {
  logger.info(Animated.Extrapolate.CLAMP);
} catch (error) {
  logger.error('react-native-reanimated failed to load Extrapolate');
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  Animated.Extrapolate = Extrapolation;
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const gcName = HermesInternal?.getRuntimeProperties();
logger.info('[Patrollar] Hermes running on:', JSON.stringify(gcName));

messaging().setBackgroundMessageHandler(async (message) => {
  logger.info('setBackgroundMessageHandler');
  logger.info(JSON.stringify(message, null, 2));
});

messaging().onMessage(((message) => {
  logger.info('onMessage');
  logger.info(JSON.stringify(message, null, 2));
}));

AppRegistry.registerComponent('Patrollar', () => Application);
