/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

const path = require('path');
const { getDefaultConfig } = require('metro-config');

const sharedLibrary = path.resolve(`${__dirname}/../shared`);
const assetsLibrary = path.resolve(`${__dirname}/../ui-components/assets`);
const componentsLibrary = path.resolve(`${__dirname}/../ui-components/mobile`);
const nodeModules = path.resolve(`${__dirname}/../../node_modules`);

const watchFolders = [
  sharedLibrary,
  assetsLibrary,
  componentsLibrary,
  nodeModules,
];

module.exports = (async () => {
  const {
    resolver: { sourceExts, assetExts },
  } = await getDefaultConfig();
  return {
    transformer: {
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: true,
        },
      }),
      babelTransformerPath: require.resolve('react-native-svg-transformer'),
    },
    resolver: {
      extraNodeModules: new Proxy({}, {
        // redirects dependencies referenced from common/ to local node_modules
        get: (target, name) => (name in target ? target[name] : path.join(process.cwd(), `../../node_modules/${name}`)),
      }),
      assetExts: assetExts.filter((ext) => ext !== 'svg'),
      sourceExts: [...sourceExts, 'svg'],
    },
    watchFolders,
  };
})();
