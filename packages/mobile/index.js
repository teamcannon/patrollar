/**
 * Necessary in order to have type checks in the main file.
 * We need an index.js to have successful production builds.
 */
require('./src/index');
