import type { IPagination } from './IPagination';

export interface IDetection {
  id: string;
  cameraId: string;
  timestamp: Date;
  url: string;
}

export type DetectionTimestampQuery = Record<'_LTE' | '_GTE', string>;

export interface DetectionsQueryVariables {
  cameraId: string;
  timestamp: DetectionTimestampQuery;
  pagination: IPagination;
}
