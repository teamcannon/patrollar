import type { IPaginationResult } from './IPagination';
import type { DetectionsQueryVariables, IDetection } from './IDetection';

export interface IDetectionService {
  get(queryVariables: DetectionsQueryVariables): Promise<IPaginationResult<IDetection>>;
}
