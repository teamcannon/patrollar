export interface ICamera {
  id: string;
  name: string;
  url: string;
  isArmed: boolean;
}

export interface ICameraDetector {
  cameraId: string;
  isAlive: boolean;
}
