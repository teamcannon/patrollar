import type { INotification, IUnreadNotificationsCount } from './INotification';
import type { IPagination, IPaginationResult } from './IPagination';

export interface INotificationService {
  get(pagination: IPagination): Promise<IPaginationResult<INotification>>;
  getUnreadCount(): Promise<IUnreadNotificationsCount[]>;
}
