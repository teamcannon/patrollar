export interface IPagination {
  id?: string;
  limit: number;
}

export interface IPaginationResult<T> {
  data: T[],
  hasNext: boolean;
}
