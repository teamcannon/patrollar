export interface INotification {
  id: string;
  isRead: boolean;
  detectionId: string;
  cameraId: string;
  url: string;
  timestamp: Date;
  message: string;
}

export interface IUnreadNotificationsCount {
  cameraId: string;
  count: number;
}
