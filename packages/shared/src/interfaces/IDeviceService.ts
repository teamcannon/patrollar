import type { IDevice, IDeviceInput } from './IDevice';

export interface IDeviceService {
  /**
   * Save user device information to the API
   *
   * @param {IDeviceInput} data - fcmToken and application type to save as device.
   * @returns {Promise<IDevice>} Newly saved device information.
   */
  save(data: IDeviceInput): Promise<IDevice>;
  /**
   * Delete user device.
   *
   * @param {string} fcmToken - Firebase Cloud Messaging token.
   * @returns {Promise<IDevice>} Deleted device information.
   */
  delete(fcmToken: string): Promise<IDevice>;
}
