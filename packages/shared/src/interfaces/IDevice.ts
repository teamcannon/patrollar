export enum ApplicationType {
  MOBILE = 'MOBILE',
  WEB = 'WEB',
  DESKTOP = 'DESKTOP',
}

export type IDeviceInput = Pick<IDevice, 'fcmToken' | 'application'>;

export interface IDevice {
  id: string;
  userId: string;
  fcmToken: string;
  application: ApplicationType;
  timestamp: Date;
}
