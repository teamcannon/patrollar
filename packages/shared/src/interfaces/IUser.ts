import type { TenantId } from '../types/TenantTypes';

export interface IUser {
  id: string;
  phoneNumber: string;
  email: string;
  tenants: TenantId[]
}
