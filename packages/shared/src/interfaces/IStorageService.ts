export interface IStorageService {
  /**
   * Stores key-value pairs into storage.
   *
   * @param {string} key - Key under which to store.
   * @param {string} value - Value to be stored.
   * @returns {Promise<string | null>}
   */
  setString(key: string, value: string): boolean | null | undefined;
  /**
   * Reads key-value from storage.
   *
   * @param {string} key - Key under which to store.
   * @returns {Promise<string | null>}
   */
  getString(key: string): string | null | undefined;
  /**
   * Removes key from storage.
   *
   * @param {string} key - Key under which to store.
   * @returns {Promise<string | null>}
   */
  remove(key: string): boolean | null | undefined;
}
