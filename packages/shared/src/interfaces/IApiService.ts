import type { DocumentNode } from 'graphql';
import type { TypedDocumentNode } from 'urql';
import type { Source } from 'wonka';
import type { CombinedError } from '@urql/core';

export type Query<Data, Variables> = DocumentNode | TypedDocumentNode<Data, Variables> | string;

export interface IApiService {
  mutation<
    Data = unknown,
    Variables = Record<string, unknown>,
  >(query: Query<Data, Variables>, variables?: Variables): Promise<Data>;
  query<
    Data = unknown,
    Variables = Record<string, unknown>,
  >(query: Query<Data, Variables>, variables?: Variables): Promise<Data>;
  subscription<
    Data = unknown,
    Variables = Record<string, unknown>,
  >(query: Query<Data, Variables>, variables?: Variables): Source<Data | CombinedError | undefined>
}
