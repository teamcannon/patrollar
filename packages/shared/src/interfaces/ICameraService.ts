import type { Source } from 'wonka';
import type { CombinedError } from '@urql/core';
import type { ICamera, ICameraDetector } from './ICamera';

export interface ICameraService {
  /**
   * Retrieves all cameras from API
   *
   * @returns {Promise<ICamera[]>} Number of armed cameras
   */
  get(): Promise<ICamera[]>;
  /**
   * Arms one or all cameras.
   *
   * @param {string | null} id - Optional id camera, if null is passed all cameras are armed
   * @returns {Promise<int>} Number of armed cameras
   */
  arm(id?: string): Promise<number>;

  /**
   * Get detector microservice status.
   *
   * @param {string|null} cameraId - Optional id camera, if null is passed all cameras statuses are returned.
   * @returns {Promise<ICameraDetector[]>} - Array of objects.
   */
  detectorStatus(cameraId?: string): Promise<ICameraDetector[]>;

  /**
   * Launches a websocket subscription that listens for camera changes.
   *
   * @return {Source<ICamera[] | CombinedError>} Subscription source
   */
  onChange(): Source<ICamera[] | CombinedError>;
}
