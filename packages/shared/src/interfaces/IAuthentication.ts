export interface IToken {
  token: string;
}

export interface IRefreshToken {
  refreshToken: string;
}

export interface ITokens extends IToken, IRefreshToken {}
