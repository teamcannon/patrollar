import type { TenantId } from '../types/TenantTypes';

export interface ITenantService {
  /**
   * Changes the current tenant id.
   * @param {string} tenantId - New tenant id.
   */
  change(tenantId: string): void;

  /**
   * Get the current tenant id.
   * @returns {Promise<TenantId | null>}
   */
  get(): TenantId | null;
}
