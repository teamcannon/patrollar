import type { subjectT } from 'wonka/dist/types/src/Wonka_types.gen';
import type { IUser } from './IUser';
import type { RegisterFormInputs } from '../schemas/RegisterForm';
import type { ITokens } from './IAuthentication';
import type { ResetPasswordFormInputs } from '../schemas/ResetPasswordForm';

export interface IUserService {
  authenticationSubject: subjectT<IUser | null>;

  me(): Promise<IUser>;
  login(email: string, password: string): Promise<IUser>;
  register(data: RegisterFormInputs): Promise<IUser>;
  logout(): Promise<void>;
  forgotPassword(email: string): Promise<void>;
  resetPassword(email: string, password: string, confirmPassword: string): Promise<IUser>;
  isAuthenticated(): boolean;
}

export interface AuthenticationResult extends ITokens {
  user: IUser;
}

export interface ResetPasswordInputs extends ResetPasswordFormInputs {
  email: string;
}
