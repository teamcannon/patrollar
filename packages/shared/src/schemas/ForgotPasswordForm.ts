import { object, string, infer as zInfer } from 'zod';

export const ForgotPasswordFormSchema = object({
  email: string().email(),
});

export type ForgotPasswordFormInputs = zInfer<typeof ForgotPasswordFormSchema>;
