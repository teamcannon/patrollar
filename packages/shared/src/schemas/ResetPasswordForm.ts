import { object, string, infer as zInfer } from 'zod';

export const ResetPasswordFormSchema = object({
  password: string().min(8)
    .regex(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      'Password must contain at least one upper letter and special character.'),
  confirmPassword: string().min(8),
}).refine(({ password, confirmPassword }) => password === confirmPassword, {
  message: 'Passwords don\'t match',
  path: ['confirmPassword'],
});

export type ResetPasswordFormInputs = zInfer<typeof ResetPasswordFormSchema>;
