import { object, string, infer as zInfer } from 'zod';

export const LoginFormSchema = object({
  email: string().email(),
  password: string().min(8),
});

export type LoginFormInputs = zInfer<typeof LoginFormSchema>;
