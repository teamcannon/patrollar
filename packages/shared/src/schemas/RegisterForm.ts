import { string, object, infer as zInfer } from 'zod';

export const RegisterFormSchema = object({
  email: string().email(),
  phoneNumber: string().min(10),
  password: string().min(8)
    .regex(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      'Password must contain at least one upper letter and special character.'),
  confirmPassword: string().min(8),
}).refine(({ password, confirmPassword }) => password === confirmPassword, {
  message: 'Passwords don\'t match',
  path: ['confirmPassword'],
});

export type RegisterFormInputs = zInfer<typeof RegisterFormSchema>;
