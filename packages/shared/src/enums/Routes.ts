enum Routes {
  login = 'login',
  register = 'register',
  forgotPassword = 'forgotPassword',
  emailConfirmation = 'emailConfirmation',
  resetPassword = 'resetPassword',
  cameras = 'cameras',
  detections = 'detections',
  notifications = 'notifications',
}

export default Routes;
