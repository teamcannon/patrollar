enum StorageKeys {
  token = 'token',
  refreshToken = 'refreshToken',
  tenantId = 'tenantId',
}

export default StorageKeys;
