import { gql } from '@urql/core';
import { inject, singleton } from 'tsyringe';
import type { IApiService } from '../interfaces/IApiService';
import type { IDevice, IDeviceInput } from '../interfaces/IDevice';
import type { IDeviceService } from '../interfaces/IDeviceService';
import InjectionTokens from './InjectionTokens';

const CreateDeviceMutation = gql<{ createDevice: IDevice }, IDeviceInput>`
    mutation ($fcmToken: String!, $application: DeviceType!) {
        createDevice(input: {
            fcmToken: $fcmToken,
            application: $application,
        }) {
            id
            timestamp
            fcmToken
            application
        }
    }
`;

const DeleteDeviceMutation = gql<{ deleteDevice: IDevice }, { fcmToken: string }>`
    mutation ($fcmToken: String!) {
        deleteDevice(fcmToken: $fcmToken) {
            id
            timestamp
            fcmToken
            application
        }
    }
`;

@singleton()
export default class DeviceService implements IDeviceService {
  constructor(@inject(InjectionTokens.ApiService) private apiService: IApiService) {}

  async save(data: IDeviceInput): Promise<IDevice> {
    const { createDevice } = await this.apiService.mutation(CreateDeviceMutation, data);
    return createDevice;
  }

  async delete(fcmToken: string): Promise<IDevice> {
    const { deleteDevice } = await this.apiService.mutation(DeleteDeviceMutation, { fcmToken });
    return deleteDevice;
  }
}
