import { inject, singleton } from 'tsyringe';
import type { ITenantService } from '../interfaces/ITenantService';
import type { TenantId } from '../types/TenantTypes';
import InjectionTokens from './InjectionTokens';
import type { IStorageService } from '../interfaces/IStorageService';
import StorageKeys from '../enums/StorageKeys';
import { logger } from '../log/Logger';

@singleton()
export default class TenantService implements ITenantService {
  private tenantLogger = logger.extend(TenantService.name);

  constructor(
    @inject(InjectionTokens.StorageService) private storageService: IStorageService,
  ) {}

  public change(tenantId: string): void {
    this.tenantLogger.debug('change', tenantId);
    this.storageService.setString(StorageKeys.tenantId, tenantId);
  }

  public get(): TenantId | null {
    this.tenantLogger.debug('get');
    return this.storageService.getString(StorageKeys.tenantId) ?? null;
  }
}
