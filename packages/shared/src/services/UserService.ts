import { gql } from '@urql/core';
import { makeSubject } from 'wonka';
import { inject, singleton } from 'tsyringe';
import type { IStorageService } from '../interfaces/IStorageService';
import type { IApiService } from '../interfaces/IApiService';
import type { AuthenticationResult, IUserService, ResetPasswordInputs } from '../interfaces/IUserService';
import type { IUser } from '../interfaces/IUser';
import type { RegisterFormInputs } from '../schemas/RegisterForm';
import type { LoginFormInputs } from '../schemas/LoginForm';
import InjectionTokens from './InjectionTokens';
import StorageKeys from '../enums/StorageKeys';
import type { ITenantService } from '../interfaces/ITenantService';

const MeQuery = gql<{ me: IUser }>`
    query {
        me {
            id
            email
            phoneNumber
            tenants
        }
    }
`;

const LoginMutation = gql<{ login: AuthenticationResult }, LoginFormInputs>`
    mutation ($email: String!, $password: String!) {
        login(input: {
            email: $email,
            password: $password
        }) {
            user {
                id
                phoneNumber
                email
                tenants
            }
            refreshToken
            token
        }
    }
`;

const RegisterMutation = gql<{ register: IUser }, RegisterFormInputs>`
    mutation ($email: String!, $phoneNumber: String!, $password: String!, $confirmPassword: String!) {
        register(input: {
            email: $email,
            phoneNumber: $phoneNumber,
            password: $password,
            confirmPassword: $confirmPassword
        }) {
            id
            phoneNumber
            email
        }
    }
`;

const ForgotPasswordMutation = gql<void, { email: string }>`
    mutation ($email: String!) {
        forgotPassword(input: {
            email: $email,
        })
    }
`;

const ResetPasswordMutation = gql<{ resetPassword: IUser }, ResetPasswordInputs>`
    mutation ($email: String!, $password: String!, $confirmPassword: String!) {
        resetPassword(input: {
            email: $email,
            password: $password,
            confirmPassword: $confirmPassword
        }) {
            id
            phoneNumber
            email
        }
    }
`;

@singleton()
export default class UserService implements IUserService {
  public authenticationSubject = makeSubject<IUser | null>();

  constructor(
    @inject(InjectionTokens.ApiService) private apiService: IApiService,
    @inject(InjectionTokens.StorageService) private storageService: IStorageService,
    @inject(InjectionTokens.TenantService) private tenantService: ITenantService,
  ) {}

  async me(): Promise<IUser> {
    const { me } = await this.apiService.query(MeQuery);
    return me;
  }

  async login(email: string, password: string): Promise<IUser> {
    // TODO handle errors here
    const variables = {
      email,
      password,
    };
    const { login } = await this.apiService.mutation(LoginMutation, variables);
    const { user, token, refreshToken } = login;
    this.storageService.setString(StorageKeys.refreshToken, refreshToken);
    this.storageService.setString(StorageKeys.token, token);
    this.tenantService.change(user.tenants[0]);
    this.authenticationSubject.next(user);
    return user;
  }

  async register(data: RegisterFormInputs): Promise<IUser> {
    const { register: user } = await this.apiService.mutation(RegisterMutation, data);

    return user;
  }

  async logout(): Promise<void> {
    this.storageService.remove(StorageKeys.token);
    this.storageService.remove(StorageKeys.refreshToken);
    this.storageService.remove(StorageKeys.tenantId);
    this.authenticationSubject.next(null);
  }

  async forgotPassword(email: string): Promise<void> {
    await this.apiService.mutation(ForgotPasswordMutation, { email });
  }

  async resetPassword(email: string, password: string, confirmPassword: string): Promise<IUser> {
    const data = {
      email,
      password,
      confirmPassword,
    };
    const { resetPassword: user } = await this.apiService.mutation(ResetPasswordMutation, data);

    return user;
  }

  isAuthenticated(): boolean {
    return !!this.storageService.getString(StorageKeys.refreshToken);
  }
}
