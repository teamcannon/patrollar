import { gql } from '@urql/core';
import { inject, singleton } from 'tsyringe';
import type { IApiService } from '../interfaces/IApiService';
import type { INotification, IUnreadNotificationsCount } from '../interfaces/INotification';
import type { INotificationService } from '../interfaces/INotificationService';
import type { IPagination, IPaginationResult } from '../interfaces/IPagination';
import InjectionTokens from './InjectionTokens';

const NotificationQuery = gql<{ notifications: INotification[] }, IPagination>`
    query($limit: Int!, $id: ID) {
        notifications(sort: { timestamp: DESCENDING }, pagination: { limit: $limit, id: $id }) {
            id
            isRead
            detectionId
            cameraId
            url
            timestamp
            message
        }
    }
`;

const UnreadNotificationsCountQuery = gql<{ unreadNotificationCount: IUnreadNotificationsCount[] }>`
    query {
        unreadNotificationCount {
            cameraId
            count
        }
    }
`;

@singleton()
export default class NotificationService implements INotificationService {
  constructor(@inject(InjectionTokens.ApiService) private apiService: IApiService) {}

  async get(pagination: IPagination): Promise<IPaginationResult<INotification>> {
    const { notifications } = await this.apiService.query(NotificationQuery, pagination);

    return {
      data: notifications,
      hasNext: !!notifications.length,
    };
  }

  async getUnreadCount(): Promise<IUnreadNotificationsCount[]> {
    const { unreadNotificationCount } = await this.apiService.query(UnreadNotificationsCountQuery);

    return unreadNotificationCount;
  }
}
