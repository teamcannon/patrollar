import type { InjectionToken } from 'tsyringe';
import type { IApiService } from '../interfaces/IApiService';
import type { IUserService } from '../interfaces/IUserService';
import type { IDeviceService } from '../interfaces/IDeviceService';
import type { INotificationService } from '../interfaces/INotificationService';
import type { IStorageService } from '../interfaces/IStorageService';
import type { ICameraService } from '../interfaces/ICameraService';
import type { IDetectionService } from '../interfaces/IDetectionService';
import type { ITenantService } from '../interfaces/ITenantService';

const InjectionTokens = {
  ApiService: Symbol.for('ApiService') as InjectionToken<IApiService>,
  UserService: Symbol.for('UserService') as InjectionToken<IUserService>,
  DeviceService: Symbol.for('DeviceService') as InjectionToken<IDeviceService>,
  NotificationService: Symbol.for('NotificationService') as InjectionToken<INotificationService>,
  StorageService: Symbol.for('StorageService') as InjectionToken<IStorageService>,
  CameraService: Symbol.for('CameraService') as InjectionToken<ICameraService>,
  DetectionService: Symbol.for('DetectionService') as InjectionToken<IDetectionService>,
  TenantService: Symbol.for('TenantService') as InjectionToken<ITenantService>,
};

export default InjectionTokens;
