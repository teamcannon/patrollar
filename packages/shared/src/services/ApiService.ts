import type { Source } from 'wonka';
import { singleton } from 'tsyringe';
import { map, pipe } from 'wonka';
import type { CombinedError } from '@urql/core';
import type { IApiService, Query } from '../interfaces/IApiService';
import ApiClient from '../connections/ApiClient';

@singleton()
export default class ApiService implements IApiService {
  constructor(
    private client: ApiClient,
  ) {}

  public async query<
    Data = unknown,
    Variables = Record<string, unknown>,
  >(query: Query<Data, Variables>, variables?: Variables): Promise<Data> {
    const { data, error } = await this.client.query(query, variables || {}).toPromise();

    return new Promise<Data>((resolve, reject) => {
      if (error) {
        return reject(error);
      }

      return resolve(<Data>data);
    });
  }

  public async mutation<
    Data = unknown,
    Variables = Record<string, unknown>,
  >(query: Query<Data, Variables>, variables?: Variables): Promise<Data> {
    const { data, error } = await this.client.mutation(query, variables || {}).toPromise();

    return new Promise<Data>((resolve, reject) => {
      if (error) {
        return reject(error);
      }

      return resolve(<Data>data);
    });
  }

  public subscription<
    Data = unknown,
    Variables = Record<string, unknown>,
  >(query: Query<Data, Variables>, variables?: Variables): Source<Data | CombinedError | undefined> {
    return pipe(
      this.client.subscription(query, variables || {}),
      map(({ error, data }) => (error || data)),
    );
  }
}
