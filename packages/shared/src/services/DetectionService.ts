import { inject, singleton } from 'tsyringe';
import { gql } from '@urql/core';
import InjectionTokens from './InjectionTokens';
import type { IApiService } from '../interfaces/IApiService';
import type { IPaginationResult } from '../interfaces/IPagination';
import type { IDetectionService } from '../interfaces/IDetectionService';
import type { DetectionsQueryVariables, IDetection } from '../interfaces/IDetection';

const DetectionsQuery = gql<{ detections: IDetection[] }, DetectionsQueryVariables>`
    query ($cameraId: ID!, $timestamp: DateFilter!, $pagination: PaginationArgs!) {
        detections(where: {
            cameraId: $cameraId, timestamp: $timestamp },
            sort: { timestamp: DESCENDING },
            pagination: $pagination
        ) {
            id
            cameraId
            timestamp
            url
        }
    }
`;

@singleton()
export default class DetectionService implements IDetectionService {
  constructor(@inject(InjectionTokens.ApiService) private apiService: IApiService) {}

  async get(queryVariables: DetectionsQueryVariables): Promise<IPaginationResult<IDetection>> {
    const { detections } = await this.apiService.query(DetectionsQuery, queryVariables);

    return {
      data: detections,
      hasNext: !!detections.length,
    };
  }
}
