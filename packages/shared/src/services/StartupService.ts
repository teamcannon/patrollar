import { container, Lifecycle } from 'tsyringe';
import ApiService from './ApiService';
import SimpleContext from '../contexts/SimpleContext';
import { IPatrollarContext, SimplePatrollarContext } from '../contexts/PatrollarContext';
import UserService from './UserService';
import type { IStorageService } from '../interfaces/IStorageService';
import DeviceService from './DeviceService';
import NotificationService from './NotificationService';
import InjectionTokens from './InjectionTokens';
import CameraService from './CameraService';
import DetectionService from './DetectionService';
import TenantService from './TenantService';

/**
 * Service responsible for the startup sequence of the apps.
 */
export default class StartupService {
  static initialize(StorageService: new () => IStorageService): void {
    container.reset();
    container
      .register(InjectionTokens.ApiService,
        { useClass: ApiService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.UserService,
        { useClass: UserService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.NotificationService,
        { useClass: NotificationService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.DeviceService,
        { useClass: DeviceService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.StorageService,
        { useClass: StorageService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.CameraService,
        { useClass: CameraService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.DetectionService,
        { useClass: DetectionService },
        { lifecycle: Lifecycle.Singleton })
      .register(InjectionTokens.TenantService,
        { useClass: TenantService },
        { lifecycle: Lifecycle.Singleton });

    const patrollarContext = SimpleContext.getContext(SimplePatrollarContext);
    const userService = container.resolve(InjectionTokens.UserService);
    const storageService = container.resolve(InjectionTokens.StorageService);
    const deviceService = container.resolve(InjectionTokens.DeviceService);
    const notificationService = container.resolve(InjectionTokens.NotificationService);
    const cameraService = container.resolve(InjectionTokens.CameraService);
    const detectionService = container.resolve(InjectionTokens.DetectionService);
    const tenantService = container.resolve(InjectionTokens.TenantService);

    Object.assign<IPatrollarContext, IPatrollarContext>(patrollarContext, {
      storageService,
      userService,
      deviceService,
      notificationService,
      cameraService,
      detectionService,
      tenantService,
    });
  }
}
