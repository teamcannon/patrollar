import { inject, singleton } from 'tsyringe';
import { CombinedError, gql } from '@urql/core';
import type { Source } from 'wonka';
import { map, pipe } from 'wonka';
import type { ICameraService } from '../interfaces/ICameraService';
import type { ICamera, ICameraDetector } from '../interfaces/ICamera';
import type { IApiService } from '../interfaces/IApiService';
import InjectionTokens from './InjectionTokens';

const CamerasQuery = gql<{ cameras: ICamera[] }>`
    query {
        cameras {
            id
            name
            url
            isArmed
        }
    }
`;

const CamerasArmMutation = gql<{ armCameras: number }, { id?: string }>`
    mutation ($id: ID) {
        armCameras(id: $id)
    }
`;

const CamerasSubscription = gql<{ cameraChange: ICamera[] }>`
    subscription {
        cameraChange {
            id
            name
            url
            isArmed
        }
    }
`;

const CamerasDetector = gql<{ detectorHeartbeat: ICameraDetector[] }, { cameraId?: string }>`
    query($cameraId: ID) {
        detectorHeartbeat(cameraId: $cameraId) {
            isAlive
            cameraId
        }
    }
`;

@singleton()
export default class CameraService implements ICameraService {
  constructor(@inject(InjectionTokens.ApiService) private apiService: IApiService) {}

  async get(): Promise<ICamera[]> {
    const { cameras } = await this.apiService.query(CamerasQuery);
    return cameras;
  }

  async arm(id?: string): Promise<number> {
    const { armCameras } = await this.apiService.mutation(CamerasArmMutation, { id });
    return armCameras;
  }

  async detectorStatus(cameraId?: string): Promise<ICameraDetector[]> {
    const { detectorHeartbeat } = await this.apiService.query(CamerasDetector, { cameraId });
    return detectorHeartbeat;
  }

  onChange(): Source<ICamera[] | CombinedError> {
    return pipe(
      this.apiService.subscription(CamerasSubscription),
      map((value) => {
        if (value instanceof CombinedError) {
          return value;
        }

        return value?.cameraChange || [];
      }),
    );
  }
}
