import { atom } from 'recoil';
// TODO find a way to avoid importing these from @patrollar/asset;shared should have no dependencies
// eslint-disable-next-line import/no-extraneous-dependencies
import { DateFilters, IDateFilter } from '@patrollar/assets/utils';

const DateFilterState = atom<IDateFilter>({
  key: 'dateFilterState',
  default: DateFilters[0],
});

// eslint-disable-next-line import/prefer-default-export
export { DateFilterState };
