import { atom } from 'recoil';
import type { IUser } from '../interfaces/IUser';

const UserState = atom<IUser>({
  key: 'userState',
  default: {} as IUser,
});

// eslint-disable-next-line import/prefer-default-export
export { UserState };
