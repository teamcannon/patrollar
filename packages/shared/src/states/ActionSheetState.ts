import { atom } from 'recoil';
import type { ReactElement } from 'react';

const ActionSheetState = atom<{ content: ReactElement | null }>({
  key: 'actionSheetState',
  default: { content: null },
});

// eslint-disable-next-line import/prefer-default-export
export { ActionSheetState };
