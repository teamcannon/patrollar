import { AtomEffect, atomFamily } from 'recoil';
import { SimplePatrollarContext } from '../contexts/PatrollarContext';
import SimpleContext from '../contexts/SimpleContext';
import type { DetectionTimestampQuery, IDetection } from '../interfaces/IDetection';
import type { IPaginationResult } from '../interfaces/IPagination';

type DetectionsStateParam = {
  cameraId: string;
  timestamp: DetectionTimestampQuery;
};

type DetectionEffectType = (param: DetectionsStateParam) => ReadonlyArray<AtomEffect<IPaginationResult<IDetection>>>;

const detectionsEffects: DetectionEffectType = ({ cameraId, timestamp }) => [
  function initializeEffect({ setSelf }) {
    const { detectionService } = SimpleContext.getContext(SimplePatrollarContext);

    setSelf(detectionService.get({ cameraId, timestamp, pagination: { limit: 5 } }));
  },
];

const DetectionsState = atomFamily<IPaginationResult<IDetection>, DetectionsStateParam>({
  key: 'detectionsState',
  default: { data: [], hasNext: false },
  effects: detectionsEffects,
});

// eslint-disable-next-line import/prefer-default-export
export { DetectionsState };
