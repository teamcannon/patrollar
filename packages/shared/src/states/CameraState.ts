import {
  atom, AtomEffect, DefaultValue, selectorFamily,
} from 'recoil';
import { CombinedError } from '@urql/core';
import { pipe, subscribe } from 'wonka';
import { SimplePatrollarContext } from '../contexts/PatrollarContext';
import SimpleContext from '../contexts/SimpleContext';
import type { ICamera } from '../interfaces/ICamera';
import { logger } from '../log/Logger';

const camerasEffect: AtomEffect<ICamera[]> = ({ setSelf }) => {
  const { cameraService } = SimpleContext.getContext(SimplePatrollarContext);

  const { unsubscribe } = pipe(
    cameraService.onChange(),
    subscribe((cameras) => {
      if (cameras instanceof CombinedError) {
        //
        logger.error('got error', cameras);
      } else {
        logger.debug('got cameras', cameras);
        setSelf((oldCameras) => {
          if (!(oldCameras instanceof DefaultValue)) {
            return oldCameras.map((c1) => ({ ...c1, ...cameras.find((c2) => c2.id === c1.id) }));
          }

          return oldCameras;
        });
      }
    }),
  );

  setSelf(cameraService.get());

  return unsubscribe;
};

const camerasDetectorState: AtomEffect<Record<string, boolean>> = ({ setSelf }) => {
  const { cameraService } = SimpleContext.getContext(SimplePatrollarContext);

  function getDetectorStatus(): Promise<Record<string, boolean>> {
    return cameraService.detectorStatus().then(
      (statuses) => statuses
        .reduce(
          (accumulator, status) => Object.assign(accumulator, { [status.cameraId]: status.isAlive }),
          {} as Record<string, boolean>,
        ),
    );
  }

  setSelf(getDetectorStatus());
};

const CamerasState = atom<ICamera[]>({
  key: 'camerasState',
  default: [],
  effects: [camerasEffect],
});

const VisibleCameraState = atom<ICamera>({
  key: 'visibleCamera',
  default: {} as ICamera,
});

const CamerasDetectorState = atom<Record<string, boolean>>({
  key: 'camerasDetectorState',
  default: {},
  effects: [camerasDetectorState],
});

const CamerasDetectorSelector = selectorFamily({
  key: 'camerasDetectorSelector',
  get: (cameraId: string) => ({ get }) => get(CamerasDetectorState)[cameraId] ?? false,
});

export {
  CamerasState, VisibleCameraState, CamerasDetectorState, CamerasDetectorSelector,
};
