import { atom, AtomEffect, selectorFamily } from 'recoil';
import { SimplePatrollarContext } from '../contexts/PatrollarContext';
import SimpleContext from '../contexts/SimpleContext';
import type { INotification } from '../interfaces/INotification';
import type { IPaginationResult } from '../interfaces/IPagination';

const notificationsEffect: AtomEffect<IPaginationResult<INotification>> = ({ setSelf }) => {
  const { notificationService } = SimpleContext.getContext(SimplePatrollarContext);

  setSelf(notificationService.get({ limit: 20 }));
};

const NotificationsState = atom<IPaginationResult<INotification>>({
  key: 'notificationsState',
  default: { data: [], hasNext: false },
  effects: [notificationsEffect],
});

const unreadNotificationsEffect: AtomEffect<Record<string, number>> = ({ setSelf }) => {
  const { notificationService } = SimpleContext.getContext(SimplePatrollarContext);

  async function initialize(): Promise<Record<string, number>> {
    const unreadNotificationCount = await notificationService.getUnreadCount();
    return unreadNotificationCount.reduce((accumulator, { cameraId, count }) => ({
      ...accumulator,
      [cameraId]: count,
    }), {} as Record<string, number>);
  }

  setSelf(initialize());
};

const UnreadNotificationsCountState = atom<Record<string, number>>({
  key: 'unreadNotificationsCountState',
  default: {},
  effects: [unreadNotificationsEffect],
});

const UnreadCameraNotificationsCountState = selectorFamily<number, string>({
  key: 'unreadCameraNotificationsCountState',
  get: (cameraId: string) => ({ get }) => {
    const unreadNotificationsCountMap = get(UnreadNotificationsCountState);

    return unreadNotificationsCountMap?.[cameraId] ?? 0;
  },
});

export { NotificationsState, UnreadCameraNotificationsCountState, UnreadNotificationsCountState };
