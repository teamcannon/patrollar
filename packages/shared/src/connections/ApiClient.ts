import {
  cacheExchange, Client, dedupExchange, fetchExchange, makeOperation, subscriptionExchange,
} from 'urql';
import { suspenseExchange } from '@urql/exchange-suspense';
import { authExchange } from '@urql/exchange-auth';
import { container, inject, singleton } from 'tsyringe';
import { gql } from '@urql/core';
import type { IRefreshToken, ITokens } from '../interfaces/IAuthentication';
import type { AuthenticationResult, IUserService } from '../interfaces/IUserService';
import InjectionTokens from '../services/InjectionTokens';
import type { IStorageService } from '../interfaces/IStorageService';
import WebsocketClient from './WebsocketClient';
import StorageKeys from '../enums/StorageKeys';
import type { ITenantService } from '../interfaces/ITenantService';

const RefreshTokenMutation = gql<{ refresh: AuthenticationResult }, IRefreshToken>`
    mutation ($refreshToken: String!) {
        refresh(input: { refreshToken: $refreshToken }) {
            user {
                id
                phoneNumber
                email
            }
            refreshToken
            token
        }
    }
`;

@singleton()
export default class ApiClient extends Client {
  constructor(
    @inject(InjectionTokens.StorageService) private storageService: IStorageService,
    @inject(InjectionTokens.TenantService) private tenantService: ITenantService,
    private websocketClient: WebsocketClient,
  ) {
    super({
      // url: 'http://localhost:3000/',
      url: 'https://api.patrollar.com/',
      suspense: true,
      exchanges: [
        dedupExchange,
        suspenseExchange,
        cacheExchange,
        authExchange<ITokens>({
          addAuthToOperation: ({ operation, authState }) => {
            if (!authState || !authState.token) {
              return operation;
            }

            const fetchOptions = typeof operation.context.fetchOptions === 'function'
              ? operation.context.fetchOptions()
              : operation.context.fetchOptions || {};

            const tenantId = this.tenantService.get();

            return makeOperation(
              operation.kind,
              operation,
              {
                ...operation.context,
                fetchOptions: {
                  ...fetchOptions,
                  headers: {
                    ...fetchOptions.headers,
                    'X-TENANT-ID': `${tenantId}`,
                    Authorization: `Bearer ${authState.token}`,
                  },
                },
              },
            );
          },
          willAuthError: ({ authState }) => !authState,
          didAuthError: ({ error }) => error.graphQLErrors.some(
            (e) => e.extensions?.code === 'UNAUTHENTICATED' || e.message === 'jwt expired',
          ),
          getAuth: async ({ authState, mutate }): Promise<ITokens | null> => {
            if (!authState) {
              return this.getTokens();
            }

            const { data } = await mutate(RefreshTokenMutation, { refreshToken: authState.refreshToken });
            if (data?.refresh) {
              const { token, refreshToken: newRefreshToken } = data.refresh;
              this.storageService.setString(StorageKeys.token, token);
              this.storageService.setString(StorageKeys.refreshToken, newRefreshToken);
              return {
                token,
                refreshToken: newRefreshToken,
              };
            }
            await this.logout();
            return null;
          },
        }),
        fetchExchange,
        subscriptionExchange({
          forwardSubscription: (operation) => ({
            subscribe: (sink) => ({
              unsubscribe: this.websocketClient.subscribe(operation, sink),
            }),
          }),
        }),
      ],
    });
  }

  private logout(): Promise<void> {
    const userService = container.resolve<IUserService>(InjectionTokens.UserService);
    return userService.logout();
  }

  private getTokens(): ITokens | null {
    const token = this.storageService.getString(StorageKeys.token);
    const refreshToken = this.storageService.getString(StorageKeys.refreshToken);

    if (token && refreshToken) {
      return {
        token,
        refreshToken,
      };
    }

    return null;
  }
}
