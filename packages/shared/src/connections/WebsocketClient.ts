import { createClient, Client } from 'graphql-ws';
import { inject, singleton } from 'tsyringe';
import type { Sink, SubscribePayload } from 'graphql-ws/lib/common';
import type { ExecutionResult } from 'graphql';
import InjectionTokens from '../services/InjectionTokens';
import type { IStorageService } from '../interfaces/IStorageService';
import StorageKeys from '../enums/StorageKeys';
import type { ITenantService } from '../interfaces/ITenantService';

@singleton()
export default class WebsocketClient {
  public client: Client;

  constructor(
    @inject(InjectionTokens.StorageService) private storageService: IStorageService,
    @inject(InjectionTokens.TenantService) private tenantService: ITenantService,
  ) {
    this.connectionParams = this.connectionParams.bind(this);
    this.client = createClient({
      // url: 'ws://localhost:3000/',
      url: 'wss://api.patrollar.com/',
      connectionParams: this.connectionParams,
    });
  }

  public subscribe<Data = Record<string, unknown>, Extensions = unknown>(payload: SubscribePayload,
    sink: Sink<ExecutionResult<Data, Extensions>>): () => void {
    return this.client.subscribe(payload, sink);
  }

  private connectionParams(): Record<string, unknown> {
    // TODO maybe get the token from somewhere else?
    // TODO what happens if the connection drops?
    // TODO probably have to call .close() when logging out and reset the connection
    // TODO what happens if the JWT token expires while connected
    const token = this.storageService.getString(StorageKeys.token);
    const tenantId = this.tenantService.get();
    return {
      Authorization: `Bearer ${token}`,
      'X-TENANT-ID': tenantId,
    };
  }
}
