export default class SimpleContext<T> {
  private value: T;

  constructor(initialValue: T) {
    this.value = initialValue;
  }

  public static createContext<U>(initialValue: U): SimpleContext<U> {
    return new SimpleContext<U>(initialValue);
  }

  public static getContext<U>(context: SimpleContext<U>): U {
    return context.value;
  }
}
