import { createContext } from 'react';
import SimpleContext from './SimpleContext';
import type { IStorageService } from '../interfaces/IStorageService';
import type { IUserService } from '../interfaces/IUserService';
import type { IDeviceService } from '../interfaces/IDeviceService';
import type { INotificationService } from '../interfaces/INotificationService';
import type { ICameraService } from '../interfaces/ICameraService';
import type { IDetectionService } from '../interfaces/IDetectionService';
import type { ITenantService } from '../interfaces/ITenantService';

export interface IPatrollarContext {
  userService: IUserService;
  storageService: IStorageService;
  deviceService: IDeviceService;
  notificationService: INotificationService;
  cameraService: ICameraService;
  detectionService: IDetectionService;
  tenantService: ITenantService;
}

export const SimplePatrollarContext = SimpleContext
  .createContext<IPatrollarContext>({} as IPatrollarContext);
export const PatrollarContext = createContext(SimpleContext.getContext(SimplePatrollarContext));
