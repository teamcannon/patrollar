import { useRecoilCallback, useRecoilValue } from 'recoil';
import { useContext } from 'react';
import { DetectionsState } from '../states/DetectionsState';
import { PatrollarContext } from '../contexts/PatrollarContext';
import type { IDetection } from '../interfaces/IDetection';
import { DateFilterState } from '../states/DateFilterState';

export type UseDetectionsReturn = [IDetection[], () => Promise<void>];

export default function useDetections(cameraId: string): UseDetectionsReturn {
  const selectedDate = useRecoilValue(DateFilterState);
  const { range: [minimumRange, maximumRange] } = selectedDate;
  const timestamp = { _GTE: minimumRange.toISOString(), _LTE: maximumRange.toISOString() };
  const { detectionService } = useContext(PatrollarContext);
  const { data: detections, hasNext } = useRecoilValue(DetectionsState({ cameraId, timestamp }));

  const loadMoreData = useRecoilCallback(({ set }) => async () => {
    if (!hasNext) return;
    const nextDetectionsBatch = await detectionService
      .get({ cameraId, timestamp, pagination: { limit: 5, id: detections[detections.length - 1].id } });
    set(DetectionsState({ cameraId, timestamp }), (currVal) => ({
      data: [...currVal.data, ...nextDetectionsBatch.data],
      hasNext: nextDetectionsBatch.hasNext,
    }));
  });

  return [
    detections,
    loadMoreData,
  ];
}
