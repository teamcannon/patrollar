import { useRecoilCallback, useRecoilValue } from 'recoil';
import { useContext } from 'react';
import { NotificationsState } from '../states/NotificationsState';
import { PatrollarContext } from '../contexts/PatrollarContext';
import type { INotification } from '../interfaces/INotification';

export type UseNotificationsReturn = [INotification[], () => Promise<void>];

export default function useNotifications(): UseNotificationsReturn {
  const { data: notifications, hasNext } = useRecoilValue(NotificationsState);
  const { notificationService } = useContext(PatrollarContext);

  const loadMoreData = useRecoilCallback(({ set }) => async () => {
    if (!hasNext) return;
    const nextNotificationBatch = await notificationService
      .get({ limit: 20, id: notifications[notifications.length - 1].id });
    set(NotificationsState, (currVal) => ({
      data: [...currVal.data, ...nextNotificationBatch.data],
      hasNext: nextNotificationBatch.hasNext,
    }));
  }, [notifications]);

  return [
    notifications,
    loadMoreData,
  ];
}
