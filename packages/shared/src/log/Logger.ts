import { consoleTransport, logger as RNLogger } from 'react-native-logs';

type LogFn = (message?: unknown, ...optionalParams: unknown[]) => void;

export interface LoggerType extends ReturnType<typeof RNLogger.createLogger> {
  debug: LogFn;
  info: LogFn
  warn: LogFn
  error: LogFn
}

export const logger = RNLogger.createLogger({
  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },
  severity: 'info',
  transport: consoleTransport,
  transportOptions: {
    colors: {
      debug: 'grey',
      info: 'blueBright',
      warn: 'yellowBright',
      error: 'redBright',
    },
  },
  async: true,
  dateFormat: 'time',
  printLevel: true,
  printDate: true,
  enabled: true,
}) as LoggerType;

