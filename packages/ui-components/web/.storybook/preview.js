import { addDecorator, addParameters } from '@storybook/react';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { Global } from '@patrollar/assets';

addParameters({
  viewport: {
    viewports: INITIAL_VIEWPORTS
  },
});

addDecorator(s => <><Global />{s()}</>);
