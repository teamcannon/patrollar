export { default as LockAnimation } from './lock-animation.json';
export { default as SirenAnimation } from './siren-animation.json';
export { default as ErrorAnimation } from './error-animation.json';
export { default as EmailAnimation } from './email-animation.json';
