export default (size: number, width = 1440): string => `${(size / width) * 100}vw`;
