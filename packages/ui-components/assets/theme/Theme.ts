import Colors from './Colors';

const Theme = {
  colors: Colors,
};

export default Theme;

export type ITheme = typeof Theme;

declare module 'styled-components' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends ITheme{}
}

declare module 'styled-components/native' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends ITheme{}
}
