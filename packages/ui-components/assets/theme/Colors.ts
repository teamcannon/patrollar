const lightColors = {
  cameraItemBackground: '#FFFFFF',
  cameraItemBorder: '#FFFFFF',
  background: '#F7F9FB',
  separator: '#F7F9FB',
};

// const darkColors = {
//   cameraItemBackground: '#2D313A',
//   cameraItemBorder: '#2D313A',
//   background: '#22242C',
//   separator: '#22242C',
// };

export default {
  primary: '#00ff4f',
  mainPage: '#dcdcdc',
  dashboardItem: 'white',
  fontColor: '#000000',
  ...lightColors,
};
