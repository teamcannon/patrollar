/* eslint-disable max-len */
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import isoWeek from 'dayjs/plugin/isoWeek';

dayjs.extend(utc);
dayjs.extend(isoWeek);

const todayStart = dayjs.utc().startOf('day');
const todayEnd = dayjs.utc().endOf('day');
const readableFormat = 'MMM DD';
const dayFormat = 'DD';

const getDateSvg = (date: string): string => `
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="147.056 155.733 329.802 313.374">
      <g transform="matrix(1, 0, 0, 1, -90.138161, 13.203779)">
          <rect y="150" stroke="currentColor" rx="50" height="300" width="300" x="250.00001" stroke-width="10" fill="none" fill-opacity="1"/>
          <rect stroke="currentColor" rx="3" id="svg_7" height="4.01537" width="200" y="207.66418" x="300.00001" fill-opacity="null" stroke-opacity="null" stroke-width="10" fill="none"/>
      </g>
      <path id="svg_10" d="m360.76923,278.46154" opacity="0.5" fill-opacity="null" stroke-opacity="null" stroke-width="10" stroke="currentColor" fill="none"/>
      <text transform="matrix(6.91123, 0, 0, 6.599949, -2101.833496, -1263.203247)" font-weight="bold" stroke="currentColor" font-family="Helvetica, Arial, sans-serif" font-size="24" y="249.86446" x="335.60456" stroke-width="0" fill="currentColor">${date}</text>
  </svg>
`;

const getLastWeekSvg = (): string => `
  <svg viewBox="147.056 155.733 329.802 313.374" xmlns="http://www.w3.org/2000/svg">
    <g transform="matrix(1, 0, 0, 1, -90.138161, 13.203779)">
      <rect y="150" stroke="currentColor" rx="50" height="300" width="300" x="250.00001" stroke-width="10" fill-opacity="1" style="fill: none;"/>
      <rect stroke="currentColor" rx="3" id="svg_7" height="4.01537" width="200" y="207.66418" x="300.00001" fill-opacity="null" stroke-opacity="null" stroke-width="10" style="fill: none;"/>
    </g>
    <path id="svg_10" d="m360.76923,278.46154" opacity="0.5" fill-opacity="null" stroke-opacity="null" stroke-width="10" stroke="currentColor" fill="none"/>
    <g transform="matrix(-0.3, 0, 0, -0.3, 385.382416, 420.274323)" fill="currentColor">
        <path d="M484.14,226.886L306.46,49.202c-5.072-5.072-11.832-7.856-19.04-7.856c-7.216,0-13.972,2.788-19.044,7.856l-16.132,16.136 c-5.068,5.064-7.86,11.828-7.86,19.04c0,7.208,2.792,14.2,7.86,19.264L355.9,207.526H26.58C11.732,207.526,0,219.15,0,234.002 v22.812c0,14.852,11.732,27.648,26.58,27.648h330.496L252.248,388.926c-5.068,5.072-7.86,11.652-7.86,18.864 c0,7.204,2.792,13.88,7.86,18.948l16.132,16.084c5.072,5.072,11.828,7.836,19.044,7.836c7.208,0,13.968-2.8,19.04-7.872 l177.68-177.68c5.084-5.088,7.88-11.88,7.86-19.1C492.02,238.762,489.228,231.966,484.14,226.886z" fill="currentColor"/>
    </g>
  </svg>
`;

const getWeekSvg = (): string => `
  <svg viewBox="147.056 155.733 329.802 313.374" xmlns="http://www.w3.org/2000/svg">
    <g transform="matrix(1, 0, 0, 1, -90.138161, 13.203779)">
      <rect y="150" stroke="currentColor" rx="50" height="300" width="300" x="250.00001" stroke-width="10" fill-opacity="1" style="fill: none;"/>
      <rect stroke="currentColor" rx="3" id="svg_7" height="4.01537" width="200" y="207.66418" x="300.00001" fill-opacity="null" stroke-opacity="null" stroke-width="10" style="fill: none;"/>
    </g>
    <path id="svg_10" d="m360.76923,278.46154" opacity="0.5" fill-opacity="null" stroke-opacity="null" stroke-width="10" stroke="currentColor" fill="none"/>
    <g transform="matrix(-0.396035, 0, 0, -0.396035, 402.542267, 434.173615)" style=""/>
    <g fill="currentColor" transform="matrix(1.12927, 0, 0, 1.12927, -27.037495, -45.230125)">
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(1.255355, 0, 0, 1.23683, -1.96389, -30.886639)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(1.255355, 0, 0, 1.23683, 43.22847, -30.886639)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(1.255355, 0, 0, 1.23683, 88.420837, -30.886639)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 595.865723, 719.778137)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 641.148621, 665.187439)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 595.986389, 665.187439)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 550.824219, 665.187439)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 505.662018, 665.187439)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 640.967529, 719.778137)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 550.763916, 719.778137)"/>
      <circle cx="220.148" cy="258.136" r="16.046" transform="matrix(-1.255355, 0, 0, -1.23683, 505.662018, 719.778137)"/>
    </g>
  </svg>
`;

export interface IDateFilter {
  id: 'today' | 'yesterday' | 'currentWeek' | 'lastWeek';
  name: string;
  range: [Date, Date];
  readableDate: string;
  getIcon: () => string;
}

const DateFilters: IDateFilter[] = [
  {
    id: 'today',
    name: 'Today',
    range: [todayStart.toDate(), todayEnd.toDate()],
    readableDate: todayStart.format(readableFormat),
    getIcon: (): string => getDateSvg(todayStart.format(dayFormat)),
  },
  {
    id: 'yesterday',
    name: 'Yesterday',
    range: [todayStart.subtract(1, 'day').toDate(), todayEnd.subtract(1, 'day').toDate()],
    readableDate: todayStart.subtract(1, 'day').format(readableFormat),
    getIcon: (): string => getDateSvg(todayStart.subtract(1, 'day').format(dayFormat)),
  },
  {
    id: 'currentWeek',
    name: 'This Week',
    range: [todayStart.startOf('isoWeek').toDate(), todayEnd.endOf('isoWeek').toDate()],
    readableDate: `${todayStart.startOf('isoWeek').format(readableFormat)} - ${todayEnd.endOf('isoWeek').format(readableFormat)}`,
    getIcon: (): string => getWeekSvg(),
  },
  {
    id: 'lastWeek',
    name: 'Last Week',
    range: [todayStart.subtract(1, 'week').startOf('isoWeek').toDate(), todayEnd.subtract(1, 'week').endOf('isoWeek').toDate()],
    readableDate: `${todayStart.subtract(1, 'week').startOf('isoWeek').format(readableFormat)} - ${todayStart.subtract(1, 'week').endOf('isoWeek').format(readableFormat)}`,
    getIcon: (): string => getLastWeekSvg(),
  },
];

export default DateFilters;
