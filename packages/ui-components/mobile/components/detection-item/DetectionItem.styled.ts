import styled from 'styled-components';
import {
  Pressable, Text, Dimensions,
} from 'react-native';
import FastImage from 'react-native-fast-image';

const StyledDetectionItemPressable = styled(Pressable)`
  align-items: center;
  margin: 10px;
  width: ${Dimensions.get('window').height * 0.25}px;
  height: ${Dimensions.get('window').height * 0.25}px;
`;

const StyledDetectionItemText = styled(Text)``;

const StyledDetectionItemImage = styled(FastImage)<{ isLoading: boolean }>`
  width: 100%;
  border-radius: 10px;
  height: ${({ isLoading }) => (isLoading ? 0 : '100%')};
`;

export { StyledDetectionItemPressable, StyledDetectionItemText, StyledDetectionItemImage };
