import { ReactElement, useState } from 'react';
import ContentLoader, { Rect } from 'react-content-loader/native';
import { useWindowDimensions } from 'react-native';
import type { IDetection } from '@patrollar/shared';
import {
  StyledDetectionItemImage,
  StyledDetectionItemPressable,
} from './DetectionItem.styled';

interface DetectionItemProps {
  item: IDetection;
  index: number;
  onDetectionPress: (detection: IDetection, index: number) => void;
}

export default function DetectionItem({ item, onDetectionPress, index }: DetectionItemProps): ReactElement {
  const [isLoading, setIsLoading] = useState(true);
  const { height } = useWindowDimensions();
  const { url } = item;

  function onLoadEnd(): void {
    return setIsLoading(false);
  }

  function onPress(): void {
    onDetectionPress(item, index);
  }

  if (!url) {
    return <></>;
  }

  return (
    <StyledDetectionItemPressable onPress={onPress}>
      {isLoading && (
        <ContentLoader backgroundColor="lightgray">
          <Rect x={0} y={0} rx={5} ry={5} width={height * 0.25} height={height * 0.25} />
        </ContentLoader>
      )}
      <StyledDetectionItemImage isLoading={isLoading} source={{ uri: url }} onLoadEnd={onLoadEnd} />
    </StyledDetectionItemPressable>
  );
}
