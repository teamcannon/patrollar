import FastImage from 'react-native-fast-image';
import type { ReactElement } from 'react';
import styled from 'styled-components';
import type { WithStyle } from '../../interfaces/WithStyle';
import { Pressable } from '../buttons/Pressable';

interface AvatarProps {
  url: string;
  width: number;
  onPress?: () => void;
}

const StyledAvatar = styled(FastImage)<WithStyle<{ width: number }>>`
  aspect-ratio: 1;
  width: ${({ width }) => width}px;
  border-radius: ${({ width }) => width}px;
  ${({ style }) => style};
`;

export default function Avatar(props: WithStyle<AvatarProps>): ReactElement {
  const {
    url, width, onPress, style,
  } = props;
  return (
    <Pressable hitSlop={10} onPress={onPress}>
      <StyledAvatar source={{ uri: url }} width={width} style={style} />
    </Pressable>
  );
}
