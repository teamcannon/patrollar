import styled from 'styled-components';
import { BaseButton } from '../base/BaseButton';

const StyledPrimaryButton = styled(BaseButton)`
  border: none;
  background-color: #4CBDAD;
`;

// eslint-disable-next-line import/prefer-default-export
export { StyledPrimaryButton };
