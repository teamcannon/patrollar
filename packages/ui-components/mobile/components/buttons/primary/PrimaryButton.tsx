import type { PropsWithChildren, ReactElement } from 'react';
import type { GestureResponderEvent } from 'react-native';
import type { FlattenSimpleInterpolation } from 'styled-components';
import { StyledPrimaryButton } from './PrimaryButton.styled';

export type PrimaryButtonProps = PropsWithChildren<{
  title?: string;
  onPress: (event: GestureResponderEvent) => void;
  style?: FlattenSimpleInterpolation;
}>;

function PrimaryButton({
  title,
  onPress,
  children,
  style,
}: PrimaryButtonProps): ReactElement {
  return (
    <StyledPrimaryButton
      style={style}
      onPress={onPress}
      title={title}
    >
      {children}
    </StyledPrimaryButton>
  );
}

export { PrimaryButton };
