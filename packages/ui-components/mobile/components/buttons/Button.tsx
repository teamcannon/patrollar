import type { ReactElement } from 'react';
import { BaseButton, BaseButtonProps } from './base/BaseButton';
import { PrimaryButtonProps, PrimaryButton } from './primary/PrimaryButton';
import { LinkButton, LinkButtonProps } from './link/LinkButton';
import { IconButton, IconButtonProps } from './icon/IconButton';

export enum ButtonTypes {
  primary = 'primary',
  default = 'default',
  link = 'link',
  icon = 'icon',
}

type ButtonProps =
  | (BaseButtonProps & { type: ButtonTypes.default })
  | (PrimaryButtonProps & { type: ButtonTypes.primary })
  | (LinkButtonProps & { type: ButtonTypes.link })
  | (IconButtonProps & { type: ButtonTypes.icon });

function Button(props: ButtonProps): ReactElement {
  const {
    type, onPress, style, children,
  } = props;
  switch (type) {
    case ButtonTypes.primary: {
      const { title } = props;
      return (
        <PrimaryButton
          onPress={onPress}
          title={title}
          style={style}
        >
          {children}
        </PrimaryButton>
      );
    }
    case ButtonTypes.link: {
      const { title } = props;
      return (
        <LinkButton
          onPress={onPress}
          title={title}
          style={style}
        >
          {children}
        </LinkButton>
      );
    }
    case ButtonTypes.icon: {
      const { icon } = props;
      return (
        <IconButton
          onPress={onPress}
          style={style}
          icon={icon}
        >
          {children}
        </IconButton>
      );
    }
    default: {
      const { title } = props;
      return (
        <BaseButton
          onPress={onPress}
          title={title}
          style={style}
        >
          {children}
        </BaseButton>
      );
    }
  }
}

export { Button };
