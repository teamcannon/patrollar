import type { PropsWithChildren, ReactElement } from 'react';
import type { GestureResponderEvent } from 'react-native';
import type { FlattenSimpleInterpolation } from 'styled-components';
import { StyledBaseText, StyledBaseButton } from './BaseButton.styled';

export type BaseButtonProps = PropsWithChildren<{
  title?: string;
  onPress: (event: GestureResponderEvent) => void;
  style?: FlattenSimpleInterpolation;
}>;

function BaseButton({
  title,
  children,
  onPress,
  style,
}: BaseButtonProps): ReactElement {
  return (
    <StyledBaseButton onPress={onPress} style={style}>
      {title ? <StyledBaseText>{title}</StyledBaseText> : children}
    </StyledBaseButton>
  );
}

export { BaseButton };
