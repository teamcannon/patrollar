import styled from 'styled-components';
import { Pressable } from '../Pressable';
import Text from '../../text/Text';
import type { WithStyle } from '../../../interfaces/WithStyle';

const StyledBaseButton = styled(Pressable).attrs(() => ({
  pressStyle: {
    transform: [{ scale: 0.98 }],
  },
}))<WithStyle>`
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  padding: 10px 30px;
  border: 1px solid gray;
  ${({ style }) => style || ''};
`;

const StyledBaseText = styled(Text)`
  color: white;
`;

export { StyledBaseButton, StyledBaseText };
