import styled from 'styled-components';
import { BaseButton } from '../base/BaseButton';

const StyledIconButton = styled(BaseButton)<{ isPrimary?: boolean }>`
  flex-direction: row;
`;

// eslint-disable-next-line import/prefer-default-export
export { StyledIconButton };
