import type { PropsWithChildren, ReactElement } from 'react';
import type { GestureResponderEvent } from 'react-native';
import type { FlattenSimpleInterpolation } from 'styled-components';
import { StyledIconButton } from './IconButton.styled';

export type IconButtonProps = PropsWithChildren<{
  onPress: (event: GestureResponderEvent) => void;
  style?: FlattenSimpleInterpolation;
  icon: ReactElement;
}>;

function IconButton({
  onPress,
  children,
  style,
  icon,
}: IconButtonProps): ReactElement {
  return (
    <StyledIconButton
      style={style}
      onPress={onPress}
    >
      {icon}
      {children}
    </StyledIconButton>
  );
}

export { IconButton };
