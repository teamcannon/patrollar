import type { PropsWithChildren, ReactElement } from 'react';
import type { GestureResponderEvent } from 'react-native';
import type { FlattenSimpleInterpolation } from 'styled-components';
import { StyledLinkButton } from './LinkButton.styled';

export type LinkButtonProps = PropsWithChildren<{
  title?: string;
  onPress: (event: GestureResponderEvent) => void;
  style?: FlattenSimpleInterpolation;
}>;

function LinkButton({
  title,
  onPress,
  children,
  style,
}: LinkButtonProps): ReactElement {
  return (
    <StyledLinkButton
      style={style}
      onPress={onPress}
      title={title}
    >
      {children}
    </StyledLinkButton>
  );
}

export { LinkButton };
