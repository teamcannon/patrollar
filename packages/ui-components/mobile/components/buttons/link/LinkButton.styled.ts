import styled from 'styled-components';
import { BaseButton } from '../base/BaseButton';

const StyledLinkButton = styled(BaseButton)`
  padding: 0;
  border-radius: 0;
  border: none;
`;

// eslint-disable-next-line import/prefer-default-export
export { StyledLinkButton };
