import {
  Pressable as RNPressable,
  PressableProps as RNPressableProps,
  StyleProp,
  ViewStyle,
} from 'react-native';
import type { FC } from 'react';

export interface PressableProps extends RNPressableProps {
  style?: StyleProp<ViewStyle>; // override to standard ViewStyle
  pressStyle?: ViewStyle; // optional style object for when pressed
  selected?: boolean;
}

type MergePressableStylesFn = (
  style?: StyleProp<ViewStyle>,
  pressStyle?: ViewStyle,
) => RNPressableProps['style'];

const mergePressableStyles: MergePressableStylesFn = (style, pressStyle) => {
  if (!pressStyle) {
    return style;
  }

  if (!style) {
    return ({ pressed }) => (pressed ? pressStyle : undefined);
  }

  return ({ pressed }) => (pressed ? [style, pressStyle] : style);
};

// eslint-disable-next-line react/prop-types
export const Pressable: FC<PressableProps> = ({ style, pressStyle, ...props }) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <RNPressable style={mergePressableStyles(style, pressStyle)} {...props} />
);
