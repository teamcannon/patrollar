import type { ReactElement } from 'react';
import { View, ViewProps } from 'react-native';

export interface BlankSeparatorProps extends ViewProps {
  height?: number;
  width?: number;
  color?: string;
}

export default function BlankSeparator({
  style,
  height,
  width,
  color: backgroundColor,
  ...otherProps
}: BlankSeparatorProps): ReactElement {
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <View style={[{ height, width, backgroundColor }, style]} {...otherProps} />
  );
}
