import { View } from 'react-native';
import styled from 'styled-components';
import type { ReactElement } from 'react';

interface SeparatorProps {
  size: number;
  horizontal?: boolean,
}

const StyledView = styled(View)<SeparatorProps>`
  width: ${({ size, horizontal }) => (!horizontal ? size : 0)}px;
  height: ${({ size, horizontal }) => (horizontal ? size : 0)}px;
  border: solid 1px ${({ theme }) => theme.colors.separator};
  align-self: center;
`;

export default function Separator({ size, horizontal = false }: SeparatorProps): ReactElement {
  return <StyledView size={size} horizontal={horizontal} />;
}
