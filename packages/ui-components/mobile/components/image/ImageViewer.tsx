import type { ReactElement } from 'react';
import type { ImageURISource } from 'react-native';
import ImageView from 'react-native-image-viewing';
import { View } from 'react-native';
import Text from '../text/Text';

interface ImageViewerProps {
  urls: ImageURISource[]
  index: number;
  visible: boolean;
  onClose: () => void;
  onChange: (index: number) => void;
}

function Footer({ index, count }: { index: number, count: number }): ReactElement {
  return (
    <View style={{ height: 64 }}>
      <Text bold inverted>{`${index + 1} / ${count}`}</Text>
    </View>
  );
}

export default function ImageViewer({
  urls, index, visible, onChange, onClose,
}: ImageViewerProps): ReactElement {
  function getFooter({ imageIndex }: { imageIndex: number }): ReactElement {
    return <Footer index={imageIndex} count={urls.length} />;
  }

  return (
    <ImageView
      swipeToCloseEnabled
      images={urls}
      imageIndex={index}
      visible={visible}
      onRequestClose={onClose}
      onImageIndexChange={onChange}
      FooterComponent={getFooter}
    />
  );
}
