import type { ReactElement } from 'react';
import { View, Animated } from 'react-native';
import { PinchGestureHandler, State, HandlerStateChangeEvent } from 'react-native-gesture-handler';

export default function Image({ url }: { url: string }): ReactElement {
  const scale = new Animated.Value(1);
  const onZoomEvent = Animated.event([{ nativeEvent: { scale } }], { useNativeDriver: true });

  function onZoomStateChange(event: HandlerStateChangeEvent): void {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      Animated.spring(scale, {
        toValue: 1,
        useNativeDriver: true,
      }).start();
    }
  }
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <PinchGestureHandler
        onGestureEvent={onZoomEvent}
        onHandlerStateChange={onZoomStateChange}
      >
        <Animated.Image
          source={{ uri: url }}
          style={{
            width: '100%',
            height: 300,
            transform: [{ scale }],
          }}
          resizeMode="contain"
        />
      </PinchGestureHandler>
    </View>
  );
}
