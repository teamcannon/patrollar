import type { ReactElement } from 'react';
import styled from 'styled-components';
import Text, { TextProps } from '../text/Text';
import type { WithStyle } from '../../interfaces/WithStyle';

interface LinkProps extends TextProps {
  prefix: string;
  link: string;
  postfix?: string;
}

const StyledLink = styled(Text)`
  color: #4CBDAD;
  ${({ style }) => style};
`;

export default function LinkText({
  prefix, link, postfix, alignment, bold, inverted, style, onPress,
}: WithStyle<LinkProps>): ReactElement {
  return (
    <Text
      alignment={alignment}
      bold={bold}
      inverted={inverted}
      style={style}
    >
      {prefix}
      <StyledLink
        onPress={onPress}
        alignment={alignment}
        bold={bold}
        inverted={inverted}
        style={style}
      >
        {' '}
        {link}
        {' '}
      </StyledLink>
      {postfix}
    </Text>
  );
}
