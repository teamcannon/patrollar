import styled, { FlattenSimpleInterpolation } from 'styled-components';
import { StyleSheet, TextInput, View } from 'react-native';
import Text from '../text/Text';
import type { WithStyle } from '../../interfaces/WithStyle';

const StyledTextInputContainer = styled(View)<{ containerStyle?: FlattenSimpleInterpolation }>`
  ${({ containerStyle }) => (containerStyle || '')};
`;

const StyledTextInput = styled(TextInput)<WithStyle<{ invalid?: boolean }>>`
  height: 40px;
  border-color: ${({ invalid }) => (invalid ? 'red' : '#757070')};
  border-width: ${StyleSheet.hairlineWidth * 2}px;
  border-radius: 10px;
  padding-left: 5px;
  ${({ style }) => (style || '')}
`;

const StyledPlaceholder = styled(Text)`
  opacity: 0.5;
`;

const StyledErrorMessage = styled(Text)`
  color: red;
`;

export {
  StyledTextInputContainer, StyledTextInput, StyledPlaceholder, StyledErrorMessage,
};
