import { ReactElement, Ref, forwardRef } from 'react';
import type { KeyboardTypeOptions, TextInput } from 'react-native';
import type { FlattenSimpleInterpolation } from 'styled-components';
import {
  StyledErrorMessage, StyledPlaceholder, StyledTextInput, StyledTextInputContainer,
} from './DefaultInput.styled';
import type { WithStyle } from '../../interfaces/WithStyle';

export interface DefaultInputProps {
  placeholder: string;
  keyboardType?: KeyboardTypeOptions;
  secureTextEntry?: boolean;
  containerStyle?: FlattenSimpleInterpolation;
}

interface InputProps extends DefaultInputProps {
  onChangeText: (text: string) => void;
  onBlur?: () => void;
  value: string;
  errorMessage?: string;
  invalid?: boolean;
}

function DefaultInput(props: WithStyle<InputProps>, ref: Ref<TextInput>): ReactElement {
  const {
    keyboardType,
    placeholder,
    onChangeText,
    secureTextEntry,
    onBlur,
    value,
    errorMessage,
    invalid,
    style,
    containerStyle,
  } = props;

  return (
    <StyledTextInputContainer containerStyle={containerStyle}>
      <StyledPlaceholder alignment="left">{placeholder}</StyledPlaceholder>
      <StyledTextInput
        onChangeText={onChangeText}
        keyboardType={keyboardType}
        onBlur={onBlur}
        value={value}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
        invalid={invalid}
        style={style}
        ref={ref}
      />
      <StyledErrorMessage alignment="left">{errorMessage}</StyledErrorMessage>
    </StyledTextInputContainer>
  );
}

export default forwardRef(DefaultInput);
