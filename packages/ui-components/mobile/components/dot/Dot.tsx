import Animated, {
  useSharedValue, withDelay, withRepeat, withTiming, Extrapolate, Easing, interpolate, useAnimatedStyle,
} from 'react-native-reanimated';
import { ReactElement, useEffect } from 'react';

interface DotProps {
  delay: number;
  repeat: boolean;
  size: number;
  color: string;
}

export default function Dot({
  delay = 0, repeat, size, color,
}: DotProps): ReactElement {
  const animation = useSharedValue(0);
  useEffect(() => {
    animation.value = withDelay(
      delay,
      withRepeat(
        withTiming(1, {
          duration: 2500,
          easing: Easing.linear,
        }),
        repeat ? -1 : 1,
        false,
      ),
    );
  }, []);
  const animatedStyles = useAnimatedStyle(() => {
    const opacity = interpolate(
      animation.value,
      [0, 1],
      [0.6, 0],
      Extrapolate.CLAMP,
    );
    return {
      opacity,
      transform: [{ scale: animation.value }],
    };
  });
  return (
    <Animated.View style={[
      {
        position: 'absolute',
        width: size,
        height: size,
        borderRadius: size / 2,
        backgroundColor: color,
      },
      animatedStyles]}
    />
  );
}
