import { Component, ReactNode } from 'react';
import Text from '../text/Text';

export default class ErrorBoundary extends Component<{ children: ReactNode }, { hasError: boolean }> {
  constructor(props: { children: ReactNode }) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(/* error: Error */): { hasError: boolean } {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(/* error: Error, errorInfo: ErrorInfo */): void {
    // TODO Maybe log the error?
  }

  render(): ReactNode {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return <Text>Something went wrong.</Text>;
    }

    return children;
  }
}
