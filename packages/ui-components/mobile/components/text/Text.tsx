import type { ReactElement, PropsWithChildren } from 'react';
import TextStyled from './Text.styled';
import type { WithStyle } from '../../interfaces/WithStyle';

export interface TextProps {
  bold?: boolean;
  inverted?: boolean;
  alignment?: 'left' | 'center' | 'right'
  onPress?: () => void;
}

export default function Text(
  props: PropsWithChildren<WithStyle<TextProps>>,
): ReactElement {
  const {
    children, style, bold, inverted, onPress, alignment = 'center',
  } = props;

  return (
    <TextStyled
      inverted={inverted}
      bold={bold}
      allowFontScaling={false}
      style={style}
      alignment={alignment}
      onPress={onPress}
    >
      {children}
    </TextStyled>
  );
}
