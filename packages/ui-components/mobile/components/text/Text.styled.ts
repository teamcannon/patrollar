import styled from 'styled-components';
import { Text } from 'react-native';
import type { WithStyle } from '../../interfaces/WithStyle';

interface TextProps {
  bold?: boolean;
  inverted?: boolean;
  alignment?: 'left' | 'center' | 'right'
}
const StyledText = styled(Text)<WithStyle<TextProps>>`
  text-align: ${({ alignment }) => alignment};
  font-weight: ${({ bold }) => (bold ? '600' : 'normal')};
  color: ${({ inverted }) => (inverted ? 'white' : 'black')};
  ${({ style }) => style};
`;

export default StyledText;
