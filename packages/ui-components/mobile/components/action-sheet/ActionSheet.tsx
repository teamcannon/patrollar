import BottomSheet, {
  BottomSheetBackdrop, BottomSheetBackdropProps,
  BottomSheetView,
  useBottomSheetDynamicSnapPoints,
} from '@gorhom/bottom-sheet';
import type {
  Ref, ReactElement, PropsWithChildren, ComponentRef, FC,
} from 'react';
import { forwardRef, useCallback, useMemo } from 'react';

function ActionSheetComponent(
  { children }: PropsWithChildren<unknown>,
  ref: Ref<BottomSheet>,
): ReactElement {
  const initialSnapPoints = useMemo(() => [0, 'CONTENT_HEIGHT'], []);

  const renderBackdrop: FC<BottomSheetBackdropProps> = useCallback((props) => (
    <BottomSheetBackdrop
      animatedIndex={props.animatedIndex}
      animatedPosition={props.animatedPosition}
      style={props.style}
      enableTouchThrough
      pressBehavior="close"
    />
  ), []);

  const {
    animatedHandleHeight,
    animatedSnapPoints,
    animatedContentHeight,
    handleContentLayout,
  } = useBottomSheetDynamicSnapPoints(initialSnapPoints);

  return (
    <BottomSheet
      ref={ref}
      index={0}
      snapPoints={animatedSnapPoints}
      handleHeight={animatedHandleHeight}
      contentHeight={animatedContentHeight}
      backdropComponent={renderBackdrop}
      enablePanDownToClose
    >
      <BottomSheetView
        onLayout={handleContentLayout}
      >
        {children}
      </BottomSheetView>
    </BottomSheet>
  );
}

export default forwardRef<ComponentRef<typeof BottomSheet>, PropsWithChildren<unknown>>(ActionSheetComponent);
