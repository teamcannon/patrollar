import styled from 'styled-components';
import { Pressable, View } from 'react-native';
import { SvgXml } from 'react-native-svg';
import Text from '../../../text/Text';

const StyledActionSheetItemContainer = styled(View)`
  width: 100%;
  padding: 12px;
`;

const StyledDatePressable = styled(Pressable)`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 0;
`;

const StyledDateName = styled(Text)<{ isSelected: boolean }>`
  font-size: 16px;
  font-weight: 700;
  color: ${({ isSelected }) => (isSelected ? '#4CBDAD' : 'black')};
`;

const StyledReadableDateName = styled(Text)`
  font-size: 14px;
  opacity: 0.5;
  font-weight: 500;
`;

const StyledTitle = styled(Text)`
  align-self: center;
  font-size: 22px;
  font-weight: bold;
  margin-bottom: 10px;
`;

const StyledDateIcon = styled(SvgXml)<{ isSelected: boolean }>`
  width: 32px;
  height: 32px;
  margin-right: 10px;
  opacity: ${({ isSelected }) => (isSelected ? 1 : 0.5)};
  color: ${({ isSelected }) => (isSelected ? '#4CBDAD' : 'black')};
`;

export {
  StyledActionSheetItemContainer,
  StyledDatePressable, StyledDateName, StyledReadableDateName, StyledTitle, StyledDateIcon,
};
