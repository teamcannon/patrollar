import { useWindowDimensions, View } from 'react-native';
import { DateFilters, IDateFilter } from '@patrollar/assets/utils';
import type { ReactElement } from 'react';
import { Check } from '@patrollar/assets/icons';
import {
  StyledActionSheetItemContainer, StyledDateIcon,
  StyledDateName,
  StyledDatePressable, StyledReadableDateName, StyledTitle,
} from './DatesFilter.styled';
import Separator from '../../../separators/Separator';

interface DatesFilterProps {
  selectedDate: IDateFilter;
  onDateSelect: (date: IDateFilter) => void;
}

export default function DatesFilter(props: DatesFilterProps): ReactElement {
  const { selectedDate, onDateSelect } = props;
  const { width } = useWindowDimensions();

  function renderDateFilter(date: IDateFilter): ReactElement {
    const {
      id, name, readableDate, getIcon,
    } = date;
    const isSelected = selectedDate.id === id;

    function onDatePress(): void {
      return onDateSelect(date);
    }

    return (
      <View key={id}>
        <StyledDatePressable onPress={onDatePress}>
          <StyledDateIcon xml={getIcon()} isSelected={isSelected} />

          <View style={{ flex: 1 }}>
            <StyledDateName alignment="left" isSelected={isSelected}>{name}</StyledDateName>
            <StyledReadableDateName alignment="left">{readableDate}</StyledReadableDateName>
          </View>

          {isSelected && (
            <View
              style={{
                width: 24,
                height: 24,
              }}
            >
              <Check color="#4CBDAD" />
            </View>
          )}
        </StyledDatePressable>
        <Separator size={width * 0.95} />
      </View>
    );
  }

  return (
    <StyledActionSheetItemContainer>
      <StyledTitle>Select a date</StyledTitle>
      {DateFilters.map(renderDateFilter)}
    </StyledActionSheetItemContainer>
  );
}
