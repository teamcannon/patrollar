import styled from 'styled-components/native';
import { flexbox, FlexboxProps } from 'styled-system';

const Box = styled.View<FlexboxProps>`
  ${flexbox}
`;

export default Box;
