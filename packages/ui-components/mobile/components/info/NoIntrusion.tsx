import type { ReactElement } from 'react';
import { StyledIcon, StyledMessage, StyledNoIntrusionContainer } from './NoIntrusion.styled';

export default function NoIntrusion(): ReactElement {
  return (
    <StyledNoIntrusionContainer>
      <StyledIcon />
      <StyledMessage>No intrusions</StyledMessage>
    </StyledNoIntrusionContainer>
  );
}
