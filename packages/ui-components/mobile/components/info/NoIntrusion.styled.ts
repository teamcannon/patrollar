import { AppLogo } from '@patrollar/assets/logos';
import styled from 'styled-components';
import { View } from 'react-native';
import Text from '../text/Text';

const StyledNoIntrusionContainer = styled(View)`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const StyledIcon = styled(AppLogo)`
  width: 200px;
  color: black;
  aspect-ratio: 2;
`;

const StyledMessage = styled(Text)`
  font-size: 24px;
  font-weight: 600;
  margin-top: 25px;
`;

export { StyledNoIntrusionContainer, StyledIcon, StyledMessage };
