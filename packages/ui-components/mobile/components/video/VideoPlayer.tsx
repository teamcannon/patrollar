import { ReactElement, useState } from 'react';
import VideoPlayer from 'react-native-video';
import { ActivityIndicator, View } from 'react-native';
import styled from 'styled-components';
import type { WithStyle } from '../../interfaces/WithStyle';

interface VideoPlayerProps extends WithStyle {
  url: string;
  resizeMode: 'stretch' | 'contain' | 'cover' | 'none'
  controls: boolean;
  isPlaying: boolean;
  poster: string;
  onReadyForDisplay?: () => void;
}

const StyledVideoPlayer = styled(VideoPlayer)<WithStyle>`
  ${({ style }) => (style || '')};
`;

export default function VideoPlayerComponent(props: VideoPlayerProps): ReactElement {
  const {
    url,
    resizeMode,
    controls,
    isPlaying,
    poster,
    style,
    onReadyForDisplay,
  } = props;
  const [isLoading, setIsLoading] = useState(true);

  function onReadyDisplay(): void {
    setIsLoading(false);
    setTimeout(() => {
      if (onReadyForDisplay) {
        onReadyForDisplay();
      }
    });
  }

  return (
    <View style={{ justifyContent: 'center' }}>
      <StyledVideoPlayer
        source={{ uri: url }}
        resizeMode={resizeMode}
        controls={controls}
        style={style}
        paused={!isPlaying}
        muted={!controls}
        onReadyForDisplay={onReadyDisplay}
        poster={poster}
        posterResizeMode={resizeMode}
      />
      <ActivityIndicator
        style={{ position: 'absolute', alignSelf: 'center' }}
        size="large"
        animating={isLoading}
        color="white"
      />
    </View>
  );
}
