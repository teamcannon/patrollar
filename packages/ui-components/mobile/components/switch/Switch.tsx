import type { ReactElement } from 'react';
import { Switch as SwitchNative } from 'react-native';

interface SwitchProps {
  value: boolean;
  onValueChange: (value: boolean) => void;
}

export default function Switch({ value, onValueChange }: SwitchProps): ReactElement {
  return (
    <SwitchNative
      trackColor={{ false: '#767577', true: '#4CBDAD' }}
      thumbColor="#f4f3f4"
      ios_backgroundColor="#767577"
      onValueChange={onValueChange}
      value={value}
    />
  );
}
