import styled from 'styled-components';
import { View } from 'react-native';
import type { ReactElement } from 'react';
import type { WithStyle } from '../../../interfaces/WithStyle';

type UnreadNotificationProps = WithStyle<{ width: number }>;

const StyledUnreadNotification = styled(View)<UnreadNotificationProps>`
  background-color: red;
  aspect-ratio: 1;
  margin-left: 10px;
  align-self: center;  
  width: ${({ width }) => width}px;
  border-radius: ${({ width }) => width}px;
  ${({ style }) => style};
`;

export default function UnreadNotification(
  { width, style }: UnreadNotificationProps,
): ReactElement {
  return <StyledUnreadNotification width={width} style={style} />;
}
