import styled from 'styled-components';
import { Pressable } from 'react-native';

const StyledNotificationContainer = styled(Pressable)`
  width: 95%;
  height: 100px;
  margin-top: 10px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  background-color: #fff;
  align-self: center;
  padding: 5px;
  border-radius: 10px;
`;

// eslint-disable-next-line import/prefer-default-export
export { StyledNotificationContainer };
