import styled from 'styled-components';
import { View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Text from '../../text/Text';

const StyledNotificationContentContainer = styled(View)`
  flex: 1;
  flex-direction: row;
`;

const StyledNotificationImage = styled(FastImage)`
  aspect-ratio: 1;
  height: 100%;
  border-radius: 10px;
`;

const StyledNotificationTitle = styled(Text)`
  font-size: 18px;
  font-weight: 700;
`;

const StyledContentSection = styled(View)`
  flex: 1;
  justify-content: space-around;
  padding: 0 10px;
`;

const StyledNotificationSubtitle = styled(Text)`
  font-size: 12px;
  opacity: 0.75;
`;

const StyledNotificationTime = styled(Text)`
  font-size: 12px;
  align-self: center;
  margin-left: auto;
`;

export {
  StyledNotificationContentContainer,
  StyledNotificationImage,
  StyledContentSection,
  StyledNotificationTitle,
  StyledNotificationSubtitle,
  StyledNotificationTime,
};
