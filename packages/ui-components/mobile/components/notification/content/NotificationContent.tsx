import type { ReactElement } from 'react';
import { View } from 'react-native';
import relativeTime from 'dayjs/plugin/relativeTime';
import dayjs from 'dayjs';
import {
  StyledContentSection,
  StyledNotificationContentContainer,
  StyledNotificationImage,
  StyledNotificationSubtitle,
  StyledNotificationTime,
  StyledNotificationTitle,
} from './NotificationContent.styled';
import UnreadNotification from '../unread/UnreadNotification';
import type { INotification } from '../../../interfaces/INotification';

// TODO check this
dayjs.extend(relativeTime);

export default function NotificationContent(
  { notification }: { notification: INotification },
): ReactElement {
  const {
    url, message, timestamp, isRead,
  } = notification;

  const formattedDate = dayjs(timestamp).fromNow();

  return (
    <StyledNotificationContentContainer>
      <StyledNotificationImage source={{ uri: url }} />
      <StyledContentSection>
        <View style={{ flexDirection: 'row' }}>
          <StyledNotificationTitle>Camera</StyledNotificationTitle>
          {!isRead && <UnreadNotification width={10} />}
          <StyledNotificationTime>{formattedDate}</StyledNotificationTime>
        </View>
        <StyledNotificationSubtitle>{message}</StyledNotificationSubtitle>
      </StyledContentSection>
    </StyledNotificationContentContainer>
  );
}
