import type { ReactElement } from 'react';
import { StyledNotificationContainer } from './Notification.styled';
import NotificationContent from './content/NotificationContent';
import type { INotification } from '../../interfaces/INotification';

interface NotificationProps<T extends INotification> {
  onNotificationPress: (notification: T, index: number) => void;
  notification: T;
  index: number;
}

export default function Notification<T extends INotification>(
  { onNotificationPress, notification, index }: NotificationProps<T>,
): ReactElement {
  function onPress(): void {
    return onNotificationPress(notification, index);
  }

  return (
    <StyledNotificationContainer onPress={onPress}>
      <NotificationContent notification={notification} />
    </StyledNotificationContainer>
  );
}
