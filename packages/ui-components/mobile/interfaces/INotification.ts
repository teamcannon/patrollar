export interface INotification {
  url: string;
  message: string;
  timestamp: Date;
  isRead: boolean;
}
