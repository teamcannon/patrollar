import type { FlattenInterpolation, ThemeProps } from 'styled-components';
import type { ITheme } from '@patrollar/assets/theme';

export type WithStyle<P = unknown> = P & { style?: FlattenInterpolation<ThemeProps<ITheme>> };
