import { TouchableNativeFeedback } from 'react-native';

export default function Button({ onPress, children }) {
  return <TouchableNativeFeedback onPress={onPress}>{children}</TouchableNativeFeedback>;
}
